'use strict';

const chessalpha = {
  WHITE: 8,
  BLACK: 16,

  EMPTY: 0x00,
  PAWN: 0x01,
  KNIGHT: 0x02,
  BISHOP: 0x03,
  ROOK: 0x04,
  QUEEN: 0x05,
  KING: 0x06,

  EPC: 0x2,
  CastleKing: 0x4,
  CastleQueen: 0x8,
  PromoteRook: 0x10,
  PromoteKnight: 0x20,
  PromoteQueen: 0x40,
  PromoteBishop: 0x80,
  Promotion: 0xf0,

  status: {
    CREATED: 10,
    ACCEPTED: 15,
    STARTED: 20,
    ABORTED: 25,
    STOPPED: 27,
    MATE: 30,
    RESIGN: 31,
    TIMEOUT: 32, // when player leaves the game
    OUTOFTIME: 33, // clock flag
    DRAW: 40,
    STALEMATE: 41,
    MOVE50: 42,
    THREEFOLD: 43,
    INSUFFICIENTMATERIAL: 44,
    UNKNOWN: 50,
  },

  result: {
    ONGOING: 0,
    DRAW: 0x04,
    WHITEWIN: 0x08,
    BLACKWIN: 0x10,
    UNKNOWN: 0x18,
  },

  nag: [
    {
      meaning: "null annotation",
    },
    {
      symbol: "!",
      meaning: "good move",
    },
    {
      symbol: "?",
      meaning: "poor move",
    },
    {
      symbol: "!!",
      meaning: "very good move",
    },
    {
      symbol: "??",
      meaning: "blunder",
    },
    {
      symbol: "!?",
      meaning: "interesting move",
    },
    {
      symbol: "?!",
      meaning: "dubious move",
    },
    {
      symbol: "□",
      meaning: "forced move",
    },
    {
      meaning: "singular move",
    },
    {
      meaning: "worst move",
    },
    {
      symbol: "=",
      meaning: "even position",
    },
    {
      meaning: "quiet position",
    },
    {
      meaning: "active position",
    },
    {
      symbol: "∞",
      meaning: "unclear position",
    },
    {
      symbol: "⩲",
      meaning: "White has a slight advantage",
    },
    {
      symbol: "⩱",
      meaning: "Black has a slight advantage",
    },
    {
      symbol: "±",
      meaning: "White has a moderate advantage",
    },
    {
      symbol: "∓",
      meaning: "Black has a moderate advantage	",
    },
    {
      symbol: "+−",
      meaning: "White has a decisive advantage",
    },
    {
      symbol: "-+",
      meaning: "Black has a decisive advantage",
    },
    {
      meaning: "White has a crushing advantage",
    },
    {
      meaning: "Black has a crushing advantage",
    },
    {
      symbol: "⨀",
      meaning: "White is in zugzwang",
    },
    {
      symbol: "⨀",
      meaning: "Black is in zugzwang",
    },
    {
      meaning: "White has a slight space advantage",
    },
    {
      meaning: "Black has a slight space advantage",
    },
    {
      meaning: "White has a moderate space advantage",
    },
    {
      meaning: "Black has a moderate space advantage",
    },
    {
      meaning: "White has a decisive space advantage",
    },
    {
      meaning: "Black has a decisive space advantage",
    },
    {
      meaning: "White has a slight time advantage",
    },
    {
      meaning: "Black has a slight time advantage",
    },
    {
      symbol: "⟳",
      meaning: "White has a moderate time advantage",
    },
    {
      symbol: "⟳",
      meaning: "Black has a moderate time advantage",
    },
    {
      meaning: "White has a decisive time advantage",
    },
    {
      meaning: "Black has a decisive time advantage",
    },
    {
      symbol: "→",
      meaning: "White has the initiative",
    },
    {
      symbol: "→",
      meaning: "Black has the initiative",
    },
    {
      meaning: "White has a lasting initiative",
    },
    {
      meaning: "Black has a lasting initiative",
    },
    {
      symbol: "↑",
      meaning: "White has the attack",
    },
    {
      symbol: "↑",
      meaning: "Black has the attack",
    },
    {
      meaning: "White has insufficient compensation for material deficit",
    },
    {
      meaning: "Black has insufficient compensation for material deficit",
    },
    {
      meaning: "White has sufficient compensation for material deficit",
    },
    {
      meaning: "Black has sufficient compensation for material deficit",
    },
    {
      meaning: "White has more than adequate compensation for material deficit",
    },
    {
      meaning: "Black has more than adequate compensation for material deficit",
    },
    {
      meaning: "White has a slight center control advantage",
    },
    {
      meaning: "Black has a slight center control advantage",
    },
    {
      meaning: "White has a moderate center control advantage",
    },
    {
      meaning: "Black has a moderate center control advantage",
    },
    {
      meaning: "White has a decisive center control advantage",
    },
    {
      meaning: "Black has a decisive center control advantage",
    },
    {
      meaning: "White has a slight kingside control advantage",
    },
    {
      meaning: "Black has a slight kingside control advantage",
    },
    {
      meaning: "White has a moderate kingside control advantage",
    },
    {
      meaning: "Black has a moderate kingside control advantage",
    },
    {
      meaning: "White has a decisive kingside control advantage",
    },
    {
      meaning: "Black has a decisive kingside control advantage",
    },
    {
      meaning: "White has a slight queenside control advantage",
    },
    {
      meaning: "Black has a slight queenside control advantage",
    },
    {
      meaning: "White has a moderate queenside control advantage",
    },
    {
      meaning: "Black has a moderate queenside control advantage",
    },
    {
      meaning: "White has a decisive queenside control advantage",
    },
    {
      meaning: "Black has a decisive queenside control advantage",
    },
    {
      meaning: "White has a vulnerable first rank",
    },
    {
      meaning: "Black has a vulnerable first rank",
    },
    {
      meaning: "White has a well protected first rank",
    },
    {
      meaning: "Black has a well protected first rank",
    },
    {
      meaning: "White has a poorly protected king",
    },
    {
      meaning: "Black has a poorly protected king",
    },
    {
      meaning: "White has a well protected king",
    },
    {
      meaning: "Black has a well protected king",
    },
    {
      meaning: "White has a poorly placed king",
    },
    {
      meaning: "Black has a poorly placed king",
    },
    {
      meaning: "White has a well placed king",
    },
    {
      meaning: "Black has a well placed king",
    },
    {
      meaning: "White has a very weak pawn structure",
    },
    {
      meaning: "Black has a very weak pawn structure",
    },
    {
      meaning: "White has a moderately weak pawn structure",
    },
    {
      meaning: "Black has a moderately weak pawn structure",
    },
    {
      meaning: "White has a moderately strong pawn structure",
    },
    {
      meaning: "Black has a moderately strong pawn structure",
    },
    {
      meaning: "White has a very strong pawn structure",
    },
    {
      meaning: "Black has a very strong pawn structure",
    },
    {
      meaning: "White has poor knight placement",
    },
    {
      meaning: "Black has poor knight placement",
    },
    {
      meaning: "White has good knight placement",
    },
    {
      meaning: "Black has good knight placement",
    },
    {
      meaning: "White has poor bishop placement",
    },
    {
      meaning: "Black has poor bishop placement",
    },
    {
      meaning: "White has good bishop placement",
    },
    {
      meaning: "Black has good bishop placement",
    },
    {
      meaning: "White has poor rook placement",
    },
    {
      meaning: "Black has poor rook placement",
    },
    {
      meaning: "White has good rook placement",
    },
    {
      meaning: "Black has good rook placement",
    },
    {
      meaning: "White has poor gueen placement",
    },
    {
      meaning: "Black has poor gueen placement",
    },
    {
      meaning: "White has good gueen placement",
    },
    {
      meaning: "Black has good gueen placement",
    },
    {
      meaning: "White has poor piece coordination",
    },
    {
      meaning: "Black has poor piece coordination",
    },
    {
      meaning: "White has good piece coordination",
    },
    {
      meaning: "Black has good piece coordination",
    },
    {
      meaning: "White has played the opening very poorly",
    },
    {
      meaning: "Black has played the opening very poorly",
    },
    {
      meaning: "White has played the opening poorly",
    },
    {
      meaning: "Black has played the opening poorly",
    },
    {
      meaning: "White has played the opening well",
    },
    {
      meaning: "Black has played the opening well",
    },
    {
      meaning: "White has played the opening very well",
    },
    {
      meaning: "Black has played the opening very well",
    },
    {
      meaning: "White has played the middlegame very poorly",
    },
    {
      meaning: "Black has played the middlegame very poorly",
    },
    {
      meaning: "White has played the middlegame poorly",
    },
    {
      meaning: "Black has played the middlegame poorly",
    },
    {
      meaning: "White has played the middlegame well",
    },
    {
      meaning: "Black has played the middlegame well",
    },
    {
      meaning: "White has played the middlegame very well",
    },
    {
      meaning: "Black has played the middlegame very well",
    },
    {
      meaning: "White has played the ending very poorly",
    },
    {
      meaning: "Black has played the ending very poorly",
    },
    {
      meaning: "White has played the ending poorly",
    },
    {
      meaning: "Black has played the ending poorly",
    },
    {
      meaning: "White has played the ending well",
    },
    {
      meaning: "Black has played the ending well",
    },
    {
      meaning: "White has played the ending very well",
    },
    {
      meaning: "Black has played the ending very well",
    },
    {
      meaning: "White has slight counterplay",
    },
    {
      meaning: "Black has slight counterplay",
    },
    {
      symbol: "⇆",
      meaning: "White has moderate counterplay",
    },
    {
      symbol: "⇆",
      meaning: "Black has moderate counterplay",
    },
    {
      meaning: "White has decisive counterplay",
    },
    {
      meaning: "Black has decisive counterplay",
    },
    {
      meaning: "White has moderate time control pressure",
    },
    {
      meaning: "Black has moderate time control pressure",
    },
    {
      meaning: "White has severe time control pressure",
    },
    {
      meaning: "Black has severe time control pressure",
    },
  ],

  /**
   * Converts a square192 to a san string
   *
   * @param {string} san
   * @returns {Number} square192
   */
  formatSquare: function(san) {
    return (san.charCodeAt(0) - 93) | ((10 - san[1]) << 4);
  },

  /**
   * Converts a san to a square192
   *
   * @param {Number} square192
   * @returns {String} san
   */
  formatSan: function(square) {
    return "abcdefgh"[(square & 0xf) - 4] + (9 - (square >> 4) + 1);
  },

  /**
   * Converts a square64 to a square192
   *
   * @param {Number} square64
   * @returns {Number} square192
   */
  format64To192: function(square64) {
    return 16 * (square64 >> 3) + (square64 & 7) + 36;
  },

  /**
   * Converts a square192 to a square64
   *
   * @param {Number} square192
   * @returns {Number} square64
   */
  format192To64: function(square) {
    return (square & (0xf - 4)) | (((square & 0xf0) >> 1) - 16);
  },

  /**
   * Converts a piece string to a piece number
   *
   * @param {String}
   * @param {String}
   * @returns {Number}
   */
  formatPiece: function(type, color) {
    return (
      chessalpha[type.toUpperCase()] |
      (color ? chessalpha[color.toUpperCase()] : 0)
    );
  },

  /**
   * Converts a piece number to a char
   *
   * @param {Number}
   * @param {Boolean}
   * @returns {String}
   */
  formatPieceToChar: function(piece, usePawn) {
    return ["", usePawn ? "p" : "", "n", "b", "r", "q", "k"][piece & 7];
  },

  /**
   * Converts a piece number to a fen char
   *
   * @param {Number}
   * @returns {String}
   */
  formatPieceToFen: function(piece) {
    return piece
      ? this.formatPieceToChar(piece & 7, true)[
          piece & chessalpha.BLACK ? "toLowerCase" : "toUpperCase"
        ]()
      : "-";
  },

  /**
   * Converts a piece number to a piece hash number (0-5)
   *
   * @param {Number}
   * @returns {Number}
   */

  formatPieceToHash: function(piece) {
    return (piece & 7) - 1 + (piece & chessalpha.BLACK) * 0.375;
  },

  /**
   * Converts a piece char to a piece number
   *
   * @param {String}
   * @returns {Number}
   */
  formatCharToPiece: function(char) {
    return {
      p: chessalpha.PAWN,
      n: chessalpha.KNIGHT,
      b: chessalpha.BISHOP,
      r: chessalpha.ROOK,
      q: chessalpha.QUEEN,
      k: chessalpha.KING,
    }[char];
  },

  /**
   * Converts a piece number to a piece string
   *
   * @param {Number}
   * @returns {String}
   */
  formatPiecestring: function(piece) {
    return ["", "pawn", "knight", "bishop", "rook", "queen", "king"][piece & 7];
  },

  /**
   * Converts a color number to a color string
   *
   * @param {Number}
   * @returns {String}
   */
  formatColorstring: function(color) {
    return ["white", "black"][(color & 24) >> 4];
  },

  /**
   * Converts a color string to a color number
   *
   * @param {String}
   * @returns {Number}
   */
  formatColor: function(colorstring) {
    return { w: chessalpha.WHITE, b: chessalpha.BLACK }[
      colorstring[0].toLowerCase()
    ];
  },

  /**
   * Converts a result string to a result number
   *
   * @param {String}
   * @returns {Number}
   */
  formatResult: function(resultstring) {
    return (
      {
        "1/2-1/2": chessalpha.result.DRAW,
        "½-½": chessalpha.result.DRAW,
        "1-0": chessalpha.result.WHITEWIN,
        "0-1": chessalpha.result.BLACKWIN,
      }[resultstring] || 0
    );
  },

  /**
   * Converts a result number to a result string
   *
   * @param {Number}
   * @returns {String}
   */
  formatResultstring: function(result) {
    return { "4": "½-½", "8": "1-0", "16": "0-1" }[result];
  },

  /**
   * Converts a square number to a xy number
   *
   * @param {Number}
   * @returns {Object} xyObject
   */
  formatXY: function(square) {
    return { x: (square & 0xf) - 4, y: (square >> 4) - 2 };
  },

  /**
   * Converts a xy number to a san square
   *
   * @param {Object} xyObject
   * @returns {String}
   */
  formatXYToSan: function(xy) {
    return ["a", "b", "c", "d", "e", "f", "g", "h"][xy.x] + (8 - xy.y);
  },

  /**
   * Converts a xy number to a square number
   *
   * @param {Object} xyObject
   * @returns {Number}
   */
  formatXYToSquare: function(xy) {
    return (xy.x + 4) | ((xy.y + 2) << 4);
  },

  /**
   * Converts a san square to a xy number
   *
   * @param {String}
   * @returns {Object} xyObject
   */
  formatSanToXY: function(san) {
    return { x: san.charCodeAt(0) - 97, y: 8 - san[1] };
  },
};

const BOARD = new Uint8Array([
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128]);

const STARTBOARD = new Uint8Array([
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128,  20,  18,  19,  21,  22,  19,  18,  20, 128, 128, 128, 128,
128, 128, 128, 128,  17,  17,  17,  17,  17,  17,  17,  17, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   9,   9,   9,   9,   9,   9,   9,   9, 128, 128, 128, 128,
128, 128, 128, 128,  12,  10,  11,  13,  14,  11,  10,  12, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128]);

const crMask = new Uint8Array([
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 7,15,15,15, 3,15,15,11, 0, 0, 0, 0,
0, 0, 0, 0,15,15,15,15,15,15,15,15, 0, 0, 0, 0,
0, 0, 0, 0,15,15,15,15,15,15,15,15, 0, 0, 0, 0,
0, 0, 0, 0,15,15,15,15,15,15,15,15, 0, 0, 0, 0,
0, 0, 0, 0,15,15,15,15,15,15,15,15, 0, 0, 0, 0,
0, 0, 0, 0,15,15,15,15,15,15,15,15, 0, 0, 0, 0,
0, 0, 0, 0,15,15,15,15,15,15,15,15, 0, 0, 0, 0,
0, 0, 0, 0,13,15,15,15,12,15,15,14, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]);


class Position {

  /***
   * Constructs position from string (empty or start), fen string or clones a Position
   * 
   * @param {String|Position}
   */
	constructor(position = "start") {
		if(position=="empty") this.position = { board: BOARD, cr: 0 };
		else if(position=="start") this.position = { board: STARTBOARD };
		else if(typeof position=="string") this.fen = position;
		else if(typeof position=="object") this.position = position;
		else throw new Error("Position constructor error")
	}

	/**
	 * @param {Position}
	 */
	set position(position) {
		this.board = (position.board||STARTBOARD).slice();
		this.eps = position.eps||-1;
		this.cr = position.cr==null ? 15:position.cr;
		this.tomove = position.tomove||chessalpha.WHITE;
		this.halfmove = position.halfmove||0;
		this.move50 = position.move50||0;
		this.hash = position.hash||this.generateHash();
	}

	/***
	 * @return {String} fen string
	 */
	get fen() {
		const board = this.board;
		const pieceArray = ["p","n","b","r","q","k"];

		let fen = "";
		let empty = 0;
		
		for(let i=0; i<64; i++) {
			let square = chessalpha.format64To192(i);
			if (!board[square]) {
        empty++;
      }
			else {
				if (empty > 0) {
					fen += empty;
					empty = 0;
				}
				let piece = pieceArray[(board[square]&7)-1];
				fen += ((board[square]&24) === chessalpha.WHITE) ? piece.toUpperCase() : piece.toLowerCase();
			}

			if(!((i + 1) & 7)) {
				if (empty > 0) {
					fen += empty;
				}
				if(i != 63) {
          fen += "/";
        }
				empty = 0;
			}
		}

		fen += this.tomove&chessalpha.WHITE ? " w ":" b ";

		if(!this.cr) fen+="-";
		else {
			if(this.cr&1) fen+="K";
			if(this.cr&2) fen+="Q";
			if(this.cr&4) fen+="k";
			if(this.cr&8) fen+="q";
		}
		fen += " ";

		fen += this.eps == -1 ? "-":chessalpha.formatSan(this.eps);
		fen += " ";

		fen += this.move50 + " ";
		
		fen += ~~(this.halfmove/2) + 1;

		return fen
	}

	/**
	 * @param {String}
	 */
	set fen(fen) {
		
		this.board = BOARD.slice();

		const chunks = fen.split(" ");
		let	square = 36;
		let pieces = chunks[0];
		let validFen = false;
		let i = 0;
		let l = pieces.length;
		
		for(i;i<l;i++) {
			const c = pieces.charAt(i);

			if(c=="/") {
				square+=8;
				validFen = true;
			}

			else {
				if (c >= "0" && c <= "9")
					square+=parseInt(c);
				else {
					let piece = c >= "a" && c <= "z" ? chessalpha.BLACK:chessalpha.WHITE;
					piece |= chessalpha.formatCharToPiece(c.toLowerCase());
					
					if((piece&24)!=piece) this.board[square] = piece;
					square++;
				}
			}
		}

		if(!validFen) throw new Error("Illegal fen")

		this.tomove = chunks[1].charAt(0) == "b" ? chessalpha.BLACK:chessalpha.WHITE;
		
		this.cr = 0;
		if(chunks[2]) {
			if(~chunks[2].indexOf("K")) this.cr |= 1;
			if(~chunks[2].indexOf("Q")) this.cr |= 2;
			if(~chunks[2].indexOf("k")) this.cr |= 4;
			if(~chunks[2].indexOf("q")) this.cr |= 8;
		}
		
		this.eps = -1;
		if (chunks[3] && chunks[3].indexOf("-") == -1) this.eps = chessalpha.formatSquare(chunks[3]);

		this.move50 = +chunks[4]||0;

		this.halfmove = (chunks[5]-1)*2;
		if(this.tomove == chessalpha.BLACK) this.halfmove++;

		this.hash = this.generateHash();
	}

	/**
	 * @returns {Array}
	 */
	get pieceList() {
		return this._pieceList()
	}

	/**
	 * @param {Number} color
	 * @returns {Array}
	 */
	_pieceList(color = 24) {
		const pieceList = [[],,,,,,,,,[],[],[],[],[],[],,,[],[],[],[],[],[]];
		const board = this.board;
		let i=192;
		while(i--) {
			if(board[i]&color) {
				pieceList[0].push(board[i]);
				pieceList[board[i]].push(i);
			}
		}
		return pieceList
	}

	/**
	 * 
	 * @param {Number} square
	 * @returns {Array} 
	 */
	pieceListFrom(square) {
		const pieceList = [[],,,,,,,,,[],[],[],[],[],[],,,[],[],[],[],[],[]];
		const board = this.board;
		if(board[square]&24) {
			pieceList[0].push(board[square]);
			pieceList[board[square]].push(square);
		}
		return pieceList
	}

	/**
	 * @param {Number}
	 * @param {Number}
	 * @param {Number} flag
	 * @returns {Number} move
	 */
	createMove(from, to, promote) {
		if(!to) [from, to, promote] = this._splitMove(from);

		if(from<36 || to<36) return from|to<<8
		const board = this.board, piece = board[from];
		let flags = 0;

		// castle
		if((piece&7)==chessalpha.KING && (from==40 || from==152)) {
			if(to-from==2 && (board[to+1]&7)==chessalpha.ROOK && !board[to] && !board[from+1]) flags |= chessalpha.CastleKing;
			else if(from-to==2 && (board[to-2]&7)==chessalpha.ROOK && !board[to-1] && !board[to] && !board[from-1]) flags |= chessalpha.CastleQueen;
		}

		// en passant
		if((piece&7)==chessalpha.PAWN && board[(to&0xF)|(from&0xF0)]==(piece^24) && board[to]==chessalpha.EMPTY) flags |= chessalpha.EPC;

		// promote
		if(promote) flags|=promote;
		else if((piece&7)==chessalpha.PAWN && ((to&0xF0)==32 || (to&0xF0)==144)) flags |= chessalpha.PromoteQueen;

		return this._generateMove(from,to,flags)
	}

	/**
	 * @param {Number} square192
	 * @param {Number} square192
	 * @param {Number} flag
	 * @returns {Number} move
	 */
	_generateMove(from, to, flags = 0) {
		return from | (to << 8) | (flags << 16)
	}

	_splitMove(move) {
		return [move & 0xFF, (move >> 8) & 0xFF, move >> 16]
	}

	get validMoves() {
		return this._validMoves()
	}

	/**
	 * @param {Number} square192
	 * @param {Number} 
	 * @param {Number}
	 * @param {Number} square192
	 */
	_validMoves(from, tomove, cr, eps) {
		return this._allMoves(from, undefined, undefined, tomove, cr, undefined, eps).filter(move => {
      const position = new Position(this);
			if((chessalpha.CastleKing|chessalpha.CastleQueen)&(move >> 16)) {
				let square = move & 0xFF;
				const oppositemoves = position._quiteMoves(undefined, undefined, undefined, position.tomove^24);
				const to = (move >> 8) & 0xFF;
        const increment = chessalpha.CastleKing&(move >> 16) ? 1:-1;
				while((square+=increment)<to) {
					if(oppositemoves.some(move => ((move>>8)&0xFF)==square)) return false
        }
			}
			position.move(move);
			return !position._inCheck(position.tomove^24)
		})
	}

	/**
	 * @returns {Array}
	 */
	get allMoves() {
		return this._allMoves()
	}

	/**
	 * @param {Number} square192
	 * @param {Array}
	 * @param {Array}
	 * @param {Array}
	 * @param {Number}
	 * @param {Number}
	 * @param {Boolean}
 	 * @param {Number} square192
	 * @returns {Array}
	 */
	_allMoves(from, moveStack = [], pieceList, board = this.board, tomove = this.tomove, cr = this.cr, inCheck = this.inCheck, eps = this.eps) {
		pieceList = pieceList||from ? this.pieceListFrom(from):this.pieceList;
		this._quiteMoves(moveStack, pieceList, this.board, tomove, cr, inCheck);
		this._captureMoves(moveStack, pieceList, this.board, tomove, eps);
		return moveStack
	}

	get quiteMoves() {
		return this._quiteMoves()
	}

	/**
	 * @param {Array}
	 * @param {Array}
	 * @param {Array}
	 * @param {Number}
	 * @param {Number}
	 * @param {Boolean}
	 * @returns {Array}
	 */
	_quiteMoves(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove, cr = this.cr, inCheck = this.inCheck) {
		this._quiteMovesPawn(moveStack, pieceList, board, tomove);
		this._quiteMovesKnight(moveStack, pieceList, board, tomove);
		this._quiteMovesBishop(moveStack, pieceList, board, tomove);
		this._quiteMovesRook(moveStack, pieceList, board, tomove);
		this._quiteMovesQueen(moveStack, pieceList, board, tomove);
		this._quiteMovesKing(moveStack, pieceList, board, tomove, cr, inCheck);

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Uint8Array} board 
	 * @param {Number} tomove 
	 * @return {Array}
	 */
	_quiteMovesPawn(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove) {
		const inc = tomove == chessalpha.WHITE ? -16 : 16;

		pieceList[chessalpha.PAWN|tomove].forEach(from => {
			let to = from + inc;
			if(board[to]==0) {
				this._movePawnTo(moveStack,from,to);

				if(((from & 0xF0) == 0x30 && tomove != chessalpha.WHITE) || ((from & 0xF0) == 0x80 && tomove == chessalpha.WHITE)) {
					to += inc;
					if (board[to]==0) {
						moveStack[moveStack.length] = this._generateMove(from, to);
					}
				}
			}
		});

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @return {Array}
	 */
	_quiteMovesKnight(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove) {
		let to;

		pieceList[chessalpha.KNIGHT|tomove].forEach(from => {
			to = from + 31; if(board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from + 33; if(board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from + 14; if(board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from - 14; if(board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from - 31; if(board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from - 33; if(board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from + 18; if(board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from - 18; if(board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to);
		});

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @return {Array}
	 */
	_quiteMovesBishop(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove) {
		let to;

		pieceList[chessalpha.BISHOP|tomove].forEach(from => {
			to = from - 15; while (board[to] == 0) { moveStack[moveStack.length] = this._generateMove(from, to); to -= 15; }
			to = from - 17; while (board[to] == 0) { moveStack[moveStack.length] = this._generateMove(from, to); to -= 17; }
			to = from + 15; while (board[to] == 0) { moveStack[moveStack.length] = this._generateMove(from, to); to += 15; }
			to = from + 17; while (board[to] == 0) { moveStack[moveStack.length] = this._generateMove(from, to); to += 17; }
		});

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @return {Array}
	 */
	_quiteMovesRook(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove) {
		let to;

		pieceList[chessalpha.ROOK|tomove].forEach(from => {
			to = from - 1; while (board[to] == 0) { moveStack[moveStack.length] = this._generateMove(from, to); to--; }
			to = from + 1; while (board[to] == 0) { moveStack[moveStack.length] = this._generateMove(from, to); to++; }
			to = from + 16; while (board[to] == 0) { moveStack[moveStack.length] = this._generateMove(from, to); to += 16; }
			to = from - 16; while (board[to] == 0) { moveStack[moveStack.length] = this._generateMove(from, to); to -= 16; }
		});

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @return {Array}
	 */
	_quiteMovesQueen(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove) {
		const localPieceList = [];
		const	queenList = pieceList[chessalpha.QUEEN|tomove];
		localPieceList[chessalpha.BISHOP|tomove] = queenList;
		localPieceList[chessalpha.ROOK|tomove] = queenList;
		this._quiteMovesBishop(moveStack, localPieceList, board, tomove);
		this._quiteMovesRook(moveStack, localPieceList, board, tomove);
		
		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @param {Number} cr
	 * @param {Boolean} inCheck
	 * @return {Array}
	 */
	_quiteMovesKing(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove, cr = this.cr, inCheck = this.inCheck) {
		let to;

		pieceList[chessalpha.KING|tomove].forEach(from => {
			to = from - 15; if (board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from - 17; if (board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from + 15; if (board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from + 17; if (board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from - 1; if (board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from + 1; if (board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from - 16; if (board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from + 16; if (board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to);

			if(!inCheck) {
        if (tomove==chessalpha.BLACK) cr >>= 2;

        if (cr & 1) {
					// Kingside castle
					if (board[from + 1] == chessalpha.EMPTY && board[from + 2] == chessalpha.EMPTY) {
						moveStack[moveStack.length] = this._generateMove(from, from + 0x02, chessalpha.CastleKing);
					}
				}
				if (cr & 2) {
          // Queenside castle
					if (board[from - 1] == chessalpha.EMPTY && board[from - 2] == chessalpha.EMPTY && board[from - 3] == chessalpha.EMPTY) {
						moveStack[moveStack.length] = this._generateMove(from, from - 0x02, chessalpha.CastleQueen);
					}
				}
			}

		});

		return moveStack
	}

	/**
	 * @returns {Array}
	 */
	get captureMoves() {
		return this._captureMoves()
	}

	/**
	 * @param {Array}
	 * @param {Array}
	 * @param {Array}
	 * @param {Number}
	 * @param {Number} square192
	 * @returns {Array}
	 */
	_captureMoves(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove, eps = this.eps) {			
		const enemy = tomove^24;

		this._captureMovesPawn(moveStack, pieceList, board, tomove, enemy, eps);
		this._captureMovesKnight(moveStack, pieceList, board, tomove, enemy);
		this._captureMovesBishop(moveStack, pieceList, board, tomove, enemy);
		this._captureMovesRook(moveStack, pieceList, board, tomove, enemy);
		this._captureMovesQueen(moveStack, pieceList, board, tomove, enemy);
		this._captureMovesKing(moveStack, pieceList, board, tomove, enemy);

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @param {Number} enemy 
	 * @param {Number} eps 
	 * @returns {Array}
	 */
	_captureMovesPawn(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove, enemy = tomove^24, eps = this.eps) {
		let inc = tomove == chessalpha.WHITE ? -16 : 16;
		let to;

		pieceList[chessalpha.PAWN|tomove].forEach(from => {
			to = from + inc - 1;
			if (board[to] & enemy) {
				this._movePawnTo(moveStack, from, to);
			}

			to = from + inc + 1;
			if (board[to] & enemy) {
				this._movePawnTo(moveStack, from, to);
			}
		});

		if (eps != -1) {
			if(board[eps-inc+1]==(chessalpha.PAWN|tomove)) moveStack[moveStack.length] = this._generateMove(eps-inc+1, eps, chessalpha.EPC);
			if(board[eps-inc-1]==(chessalpha.PAWN|tomove)) moveStack[moveStack.length] = this._generateMove(eps-inc-1, eps, chessalpha.EPC);
		}

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @param {Number} enemy 
	 * @returns {Array}
	 */
	_captureMovesKnight(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove, enemy = tomove^24) {
		let to;

		pieceList[chessalpha.KNIGHT|tomove].forEach(from => {
			to = from + 31; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from + 33; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from + 14; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from - 14; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from - 31; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from - 33; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from + 18; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from - 18; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
		});

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @param {Number} enemy 
	 * @returns {Array}
	 */
	_captureMovesBishop(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove, enemy = tomove^24) {
		let to;

		pieceList[chessalpha.BISHOP|tomove].forEach(from => {
			to = from; do { to -= 15; } while (board[to] == 0); if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from; do { to -= 17; } while (board[to] == 0); if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from; do { to += 15; } while (board[to] == 0); if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from; do { to += 17; } while (board[to] == 0); if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
		});

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @param {Number} enemy 
	 * @return {Array}
	 */
	_captureMovesRook(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove, enemy = tomove^24) {
		let to;

		pieceList[chessalpha.ROOK|tomove].forEach(from => {
			to = from; do { to--; } while (board[to] == 0); if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from; do { to++; } while (board[to] == 0); if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from; do { to -= 16; } while (board[to] == 0); if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from; do { to += 16; } while (board[to] == 0); if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
		});

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @param {Number} enemy 
	 * @returns {Array}
	 */
	_captureMovesQueen(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove, enemy = tomove^24) {
		const localPieceList = [];
		const	queenList = pieceList[chessalpha.QUEEN|tomove];
		localPieceList[chessalpha.BISHOP|tomove] = queenList;
		localPieceList[chessalpha.ROOK|tomove] = queenList;
		this._captureMovesBishop(moveStack, localPieceList, board, tomove, enemy);
		this._captureMovesRook(moveStack, localPieceList, board, tomove, enemy);
		
		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @param {Number} enemy 
	 * @returns {Array}
	 */
	_captureMovesKing(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove, enemy = tomove^24) {
		let to;
	
		pieceList[chessalpha.KING|tomove].forEach(from => {
			to = from - 15; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from - 17; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from + 15; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from + 17; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from - 1; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from + 1; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from - 16; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
			to = from + 16; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to);
		});

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Number} from 
	 * @param {Number} to 
	 */
	_movePawnTo(moveStack, from, to) {
		const row = to&0xF0;
		if(row==0x90 || row==0x20) {
			moveStack[moveStack.length] = this._generateMove(from, to, chessalpha.PromoteQueen);
			moveStack[moveStack.length] = this._generateMove(from, to, chessalpha.PromoteRook);
			moveStack[moveStack.length] = this._generateMove(from, to, chessalpha.PromoteBishop);
			moveStack[moveStack.length] = this._generateMove(from, to, chessalpha.PromoteKnight);
		}
		else moveStack[moveStack.length] = this._generateMove(from, to);
	}

	/**
	 * @return {Boolean}
	 */
	get inCheck() {
		return this._inCheck()
	}

	/**
	 * @param {Number} 
	 * @param {Array}
	 * @returns {Boolean}
	 */
	_inCheck(tomove = this.tomove, oppositemoves = this._captureMoves(undefined, undefined, undefined, tomove^24)) {
		const king = this.board.indexOf(chessalpha.KING|tomove);
		return oppositemoves.some(move => { return (move>>8&0xFF)==king })
	}

	/**
	 * @returns {Boolean}
	 */
	get checkmate() {
		return this._checkmate()
	}

	/**
	 * @param {Number} 
	 * @param {Array}
	 * @returns {Boolean}
	 */
	_checkmate(validmoves = this.validMoves, oppositemoves) {
		return !validmoves.length && this._inCheck(undefined, oppositemoves)
	}

	/**
	 * @returns {Boolean}
	 */
	get stalemate() {
		return this._stalemate()
	}

	/**
	 * @param {Number} 
	 * @param {Array}
	 * @returns {Boolean}
	 */
	_stalemate(validmoves = this.validMoves, oppositemoves) {
		return !validmoves.length && !this._inCheck(undefined, oppositemoves)
	}

	/**
	 * @returns {Boolean}
	 */
	get insufficientMaterial() {
		return this._insufficientMaterial()
	}

	/**
	 * 
	 * @param {Array} pieceList 
	 * @returns {Boolean}
	 */
	_insufficientMaterial(pieceList = this.pieceList) {
		const	n_pieces = pieceList[0].length;
		const n_bishops = pieceList[chessalpha.BISHOP|chessalpha.WHITE].length + pieceList[chessalpha.BISHOP|chessalpha.BLACK].length;
		const n_knights = pieceList[chessalpha.KNIGHT|chessalpha.WHITE].length + pieceList[chessalpha.KNIGHT|chessalpha.BLACK].length;

		// k vs k
		if(n_pieces==2) return true

		// k vs kn ... or ... k vs kb
		else if (n_pieces === 3 && (n_bishops === 1 || n_knights === 1)) return true

		// kb vs kb where bishops are on the same color
		else if (n_pieces === 4 && n_bishops === 2) {
			const whiteSquare = pieceList[chessalpha.BISHOP|chessalpha.WHITE][0];
			const	blackSquare = pieceList[chessalpha.BISHOP|chessalpha.BLACK][0];
			if(whiteSquare && blackSquare) return ((whiteSquare&16)>>4)==(whiteSquare&1) === ((blackSquare&16)>>4)==(blackSquare&1)
		}

		return false
	}

	/**
	 * @returns {Number}
	 */
	get status() {
		return this._status()
	}

	/**
	 * 
	 * @param {Array} validmoves 
	 * @param {Array} oppositemoves 
	 * @returns {Number}
	 */
	_status(validmoves = this.validMoves, oppositemoves = this._captureMoves(undefined, undefined, undefined, this.tomove^24)) {
		return this._stalemate(validmoves, oppositemoves) ? chessalpha.status.STALEMATE:
			this.insufficientMaterial ? chessalpha.status.INSUFFICIENTMATERIAL:
			this._checkmate(validmoves, oppositemoves) ? chessalpha.status.MATE:
			this.move50 > 100 ? chessalpha.status.MOVE50:0
	}

	/**
	 * 
	 * @param {Number|String} move 
	 * @returns {Position}
	 */
	move(move) {
		if(typeof(move) === 'string') move = this.sanToMove(move);

		this.halfmove += 1;
		this.move50 += 1;

		if(move===1) return this

		this.hash.update(12 * 64 + 1);
		if(this.tomove!=24) this.tomove ^= 24;

		if(move===0) return this
		
		const [ from, to, flags ] = this._splitMove(move);
		const board = this.board;
		const from64 = chessalpha.format192To64(from);
		const to64 = chessalpha.format192To64(to);

		// insert piece, from = piece
		if(from<36) {
			if(board[to]) this.hash.update(chessalpha.formatPieceToHash(board[to])*64+to64);
			this.hash.update(chessalpha.formatPieceToHash(from)*64+to64);

			board[to] = from;

			this.move50 = 0;
			return this
		}

		// remove piece from square, not needed due to insert piece 0?
		else if(to<36) {
			if(board[from]) this.hash.update(chessalpha.formatPieceToHash(board[from])*64+to64);

			board[from] = chessalpha.EMPTY;

			this.move50 = 0;
			return this
		}

		let piece = board[from];
		const color = piece&24;

		if(flags) {
			if(flags&chessalpha.CastleKing) {
				this.hash.update(chessalpha.formatPieceToHash(board[to+1])*64+to64-1);
				this.hash.update(chessalpha.formatPieceToHash(board[to+1])*64+to64+1);

				board[to-1] = board[to+1];
				board[to+1] = chessalpha.EMPTY;
			}
			if(flags&chessalpha.CastleQueen) {
				this.hash.update(chessalpha.formatPieceToHash(board[to-2])*64+to64+1);
				this.hash.update(chessalpha.formatPieceToHash(board[to-2])*64+to64-2);

				board[to+1] = board[to-2];
				board[to-2] = chessalpha.EMPTY;
			}
			if(flags&chessalpha.EPC) {
				const square = from&0xF0|to&0xF;
				this.hash.update(chessalpha.formatPieceToHash(board[square])*64+chessalpha.format192To64(square));
				board[square] = chessalpha.EMPTY;
			}
			if(flags&chessalpha.Promotion) {
				if(flags&chessalpha.PromoteQueen) piece = color|chessalpha.QUEEN;
				else if(flags&chessalpha.PromoteBishop) piece = color|chessalpha.BISHOP;
				else if(flags&chessalpha.PromoteKnight) piece = color|chessalpha.KNIGHT;
				else piece = color|chessalpha.ROOK;
			}
			this.move50 = 0;
		}

		if(board[to]||(piece&7)==chessalpha.PAWN) this.move50 = 0;

		this.hash.update(chessalpha.formatPieceToHash(board[from])*64+from64);
		this.hash.update(chessalpha.formatPieceToHash(piece)*64+to64);

		board[to] = piece;
		board[from] = chessalpha.EMPTY;

		if(~this.eps) this.hash.update(12 * 64 + 1 + 4 + this.eps&0xF - 4);
		this.eps = (piece&7)==chessalpha.PAWN && Math.abs(from-to)==32 ? (from+to)/2:-1;
		if(~this.eps) this.hash.update(12 * 64 + 1 + 4 + this.eps&0xF - 4);

		if(this.cr&1) this.hash.update(12 * 64 + 1 + 1);
		if(this.cr&2) this.hash.update(12 * 64 + 1 + 2);
		if(this.cr&4) this.hash.update(12 * 64 + 1 + 3);
		if(this.cr&8) this.hash.update(12 * 64 + 1 + 4);
		this.cr &= crMask[from] & crMask[to];
		if(this.cr&1) this.hash.update(12 * 64 + 1 + 1);
		if(this.cr&2) this.hash.update(12 * 64 + 1 + 2);
		if(this.cr&4) this.hash.update(12 * 64 + 1 + 3);
		if(this.cr&8) this.hash.update(12 * 64 + 1 + 4);
		
		return this
	}

	/**
	 * 
	 * @param {String} san 
	 * @param {Array} validmoves
	 * @returns {Number} 
	 */
	sanToMove(san, validmoves = this.validMoves) {
		san = san.replace(/(\W|x)*/g,'');

		if(san == "OO" || san == "00") {
			const from = this.tomove==chessalpha.BLACK ? 40:152;
			return this._generateMove(from, from + 2, chessalpha.CastleKing)
		}
		if(san == "OOO" || san == "000") {
			const from = this.tomove==chessalpha.BLACK ? 40:152;
			return this._generateMove(from, from - 2, chessalpha.CastleQueen)
		}

		san = san[0] + san.slice(1).toLowerCase();
		
		const matches = san.match(/([PNBRQK])?([a-h])?([1-8])?([a-h][1-8])([qrbn])?/);
		const line = matches[2] ? matches[2].charCodeAt(0) - 93:0;
		const	row = matches[3] ? 10-matches[3]<<4:0;
    const to = chessalpha.formatSquare(matches[4]);
		const promotion = ({n:chessalpha.PromoteKnight,b:chessalpha.PromoteBishop,r:chessalpha.PromoteRook,q:chessalpha.PromoteQueen})[matches[5]]||0;
		
		let piece = chessalpha.formatPiecestring(chessalpha.formatCharToPiece((matches[1]||'p').toLowerCase()));
		piece = piece.charAt(0).toUpperCase() + piece.slice(1);

		const pieceList = line && row ? this.pieceListFrom(line|row):this.pieceList;
		const	moves = this['_captureMoves' + piece](this['_quiteMoves' + piece]([], pieceList), pieceList).filter(move => { return !new Position(this).move(move)._inCheck(this.tomove) });

		let i = moves.length;
		if(line && row) { while(i--) if(((moves[i]>>8)&0xFF) == to && (moves[i]&0xF) == line && (moves[i]&0xF0) == row && ((moves[i]>>16)&chessalpha.Promotion)==promotion) return moves[i] }
		else if(line) { while(i--) if(((moves[i]>>8)&0xFF) == to && (moves[i]&0xF) == line && ((moves[i]>>16)&chessalpha.Promotion)==promotion) return moves[i] }
		else if(row) { while(i--) if(((moves[i]>>8)&0xFF) == to && (moves[i]&0xF0) == row && ((moves[i]>>16)&chessalpha.Promotion)==promotion) return moves[i] }
		else { while(i--) if(((moves[i]>>8)&0xFF) == to && ((moves[i]>>16)&chessalpha.Promotion)==promotion) return moves[i] }

    return null
	}

	/**
	 * 
	 * @param {Number} move 
	 * @param {Array} validmoves 
	 * @returns {String}
	 */
	moveToSan(move, validmoves = this.validMoves) {
		if(move==0) return "--"
		if(move==1) return "*"

		const [ from, to, flags ] = this._splitMove(move);

		if(from<36) return chessalpha.formatPieceToChar(from&7).toUpperCase() + "@" + chessalpha.formatSan(to)
		if(to<36) return "-" + chessalpha.formatSan(from)

    if(!~validmoves.indexOf(move)) return chessalpha.formatSan(from) + chessalpha.formatSan(to) + (flags & chessalpha.Promotion ? ({"16":"=R","32":"=N","64":"=Q","128":"=B"})[flags&chessalpha.Promotion]:'')

		if (flags & chessalpha.CastleKing) return "O-O"
		if (flags & chessalpha.CastleQueen) return "O-O-O"

		const board = this.board;
		const	piece = board[from];

		let san = chessalpha.formatPieceToChar(piece&7).toUpperCase();
    let dupe = false;
    let rowDiff = true;
    let colDiff = true;
	  let i = validmoves.length;

		while(i--) {
			const moveFrom = validmoves[i] & 0xFF;
			const	moveTo = (validmoves[i] >> 8) & 0xFF;
			if (moveFrom != from && moveTo == to && board[moveFrom] == piece) {
				dupe = true;
				if ((moveFrom & 0xF0) == (from & 0xF0)) {
					rowDiff = false;
				}
				if ((moveFrom & 0x0F) == (from & 0x0F)) {
					colDiff = false;
				}
			}
		}

		const conflict = chessalpha.formatSan(from);
		if (dupe) san += colDiff ? conflict[0]:rowDiff ? conflict[1]:conflict;
		else if ((piece&7) == chessalpha.PAWN && (board[to] != 0 || (flags & chessalpha.EPC))) san += conflict[0];

		if (board[to] != 0 || (flags & chessalpha.EPC)) san += "x";

		san += chessalpha.formatSan(to);

		if (flags & chessalpha.Promotion) san += ({"16":"=R","32":"=N","64":"=Q","128":"=B"})[flags&chessalpha.Promotion];

		const position = new Position(this).move(move);
		if(position.inCheck) san += position.validMoves.length == 0 ? "#" : "+";

		return san
	}

	/**
	 * @returns {String}
	 */
	get ascii() {
		let square = 0, piece, s = '\n   +------------------------+\n';
		for(let row=0; row<8; row++) {
			s += ' '+(8-row)+' |';
			for(let col=0; col<8; col++) {
				piece = this.board[16 * (square>>3) + (square&7) + 36];
				if(!piece) s += ' . ';
				else s += ' '+['p','n','b','r','q','k'][(piece&7)-1][piece&chessalpha.BLACK ? 'toLowerCase':'toUpperCase']()+' ';
				square++;
			}
			s += '|\n';
		}
		s += '   +------------------------+\n';
		s += '     a  b  c  d  e  f  g  h\n';
		return s
	}

	/**
	 * @returns {String}
	 */
	get key() {
		let key = "";
		for(let i = 0; i<64; i++)
			key += chessalpha.formatPieceToFen(this.board[chessalpha.format64To192(i)]);
		key += String.fromCharCode(
			this.cr+97,
			this.eps&0xF-4+97,
			108.5-(this.tomove-12)/(4/10.5)
		);
		return key
	}

	/**
	 * @returns {Hash}
	 */
	generateHash() {
		const hash = new Hash();
		let piece;
		let i = 64;
		while(i--) {
			piece = this.board[chessalpha.format64To192(i)];
			if(piece) hash.update(chessalpha.formatPieceToHash(piece)*64+i);
		}

		if(this.tomove===chessalpha.BLACK) hash.update(12 * 64 + 1);

		if(this.cr&1) hash.update(12 * 64 + 1 + 1);
		if(this.cr&2) hash.update(12 * 64 + 1 + 2);
		if(this.cr&4) hash.update(12 * 64 + 1 + 3);
		if(this.cr&8) hash.update(12 * 64 + 1 + 4);

		if(~this.eps) hash.update(12 * 64 + 1 + 4 + this.eps&0xF - 4);

		return hash
	}

}

/**
 * 
 * @param {Number} seed 
 * @returns {Number}
 */
function Random(seed) {
  const c = 4294967295;

	this._seed = seed % c;

	// Returns a pseudo-random value between 1 and 2^32 - 2.
	this.next = multiple => {
		return this._seed = this._seed * multiple % c
	};
}

class Hash {

	/**
	 * 
	 * @param {Number} low 
	 * @param {Number} high 
	 */
	constructor(low, high) {
		let count = 12 * 64 + 1 + 4 + 8; // pieces * squares + tomove + castlerights + ep lines
		const lowRandom = new Random(0x1BADF00D);
		const highRandom = new Random(0xC11E55);
		this.lowKeys = new Uint32Array(count);
		this.highKeys = new Uint32Array(count);
		
		while(count--) {
			this.lowKeys[count] = lowRandom.next(16807 + count);
			this.highKeys[count] = highRandom.next(16807 - count);
		}

		this.low = 0;
		this.high = 0;
	}

	/**
	 * 
	 * @param {Number} value 
	 */
	update(value) {
		this.low ^= this.lowKeys[value];
		this.high ^= this.highKeys[value];
	}

	/**
	 * 
	 * @param {Hash} hash 
	 * @returns {Boolean}
	 */
	equals(hash) {
		return this.low == hash.low && this.high == hash.high
	}

	/**
	 * @returns {String}
	 */
	toString() {
		return `${this.low},${this.high}`
	}
}

class Movelist {

	/**
	 * 
	 * @param {Array|Movelist} movelist 
	 */
	constructor(movelist) {
		this.list = [];

		if(movelist) {
			//problem om endast 1 ställning
			if(movelist[0] && movelist[0].next && typeof movelist[0].next[0] === 'number') this.toPointers(movelist,0);
			else this.list = (movelist.list||movelist).map(node => new Node(node));
		}
	}

	/**
	 * 
	 * @param {Array} list 
	 * @param {Node} node 
	 * @param {Node} prev
	 * @returns {Node} 
	 */
	toPointers(list, node, prev) {
		if(typeof node === 'number') node = list[node];
		let next = node.next||[];
		node.id = this.list.length;
		if(node.prev == null && prev) node.prev = prev;
		node = this.save(node);
		for(let i=0; i<next.length; i++) this.toPointers(list, next[i], node);
		return node
	}

	/**
	 * 
	 * @param {Node|Object} object 
	 * @param {Number} index 
	 * @returns {Node}
	 */
	save(object, index) {
		object.id = object.id||this.list.length;
		const node = new Node(object);
		
		if(object.prev && object.prev.next)
			object.prev.next.splice(index!=null ? index:object.prev.next.length, 0, node);
		this.list[node.id] = node;
		return node
	}

	/**
	 * 
	 * @param {Number} move 
	 * @param {Node} prev 
	 * @param {String} san 
	 * @param {Number} index 
	 * @returns {Node}
	 */
	move(move, prev, san, index) {
		if(index==null) {
			let next = prev.next;
			let	i = next.length;
			while(i--) if(move === next[i].move) return next[i]
		}
		return this.save({move, prev, san, timestamp: Date.now()}, index)
	}

	/**
	 * @deprecated
	 * @param {Node} node 
	 * @returns {Position}
	 */
	position(node) {
		console.warn("Deprecated, use Node.position instead of Movelist.position");
		return node.position
	}

	/**
	 * 
	 * @param {Node} node 
	 * @returns {Node}
	 */
	first(node) {
		return node ? node.first:this.list[0]
	}

	/**
	 * 
	 * @param {Node} node 
	 * @returns {Node}
	 */
	last(node) {
		return (node||this.list[0]).last
	}

	/**
	 * 
	 * @param {Node|Number} node 
	 */
	remove(node) {
		if(typeof node == 'number') node = this.list[node];
		node.remove(node => {
			delete this.list[node.id];
		});
	}

	/**
	 * 
	 * @param {Node} node1 
	 * @param {Node} node2 
	 * @param {Boolean} prepend 
	 * @returns {Movelist}
	 */
	_merge(node1, node2, prepend) {
		const nodesToAdd = [];
		for(let i=0;i<node2.next.length;i++) {
			for(let j=0;j<node1.next.length;j++) {
				if(node1.next[j].equals(node2.next[i])) this._merge(node1.next[i],node2.next[i],prepend);
				else nodesToAdd.push(node2.next[i]);
			}
		}

		for(let i=0;i<nodesToAdd.length;i++) {
			nodesToAdd[i].id = null;
			this.save(nodesToAdd[i]);
		}

		node1.next = prepend ? nodesToAdd.concat(node1.next):node1.next.concat(nodesToAdd);

		return this
	}

	/**
	 * 
	 * @param {Node} node1 
	 * @param {Node} node2 
	 * @param {Boolean} prepend 
	 * @returns {Movelist}
	 */
	merge(node1, node2, prepend) {
		return node1.position.hash.equals(node2.position.hash) ? this._merge(node1, node2, prepend):null
	}

	/**
	 * @returns {Object}
	 */
	toJSON() {
		return this.list.map(node => node.toJSON())
	}
}


class Node {

	/**
	 * @param {Node} node 
	 */
	constructor(node) {
		Object.assign(this,node);
		this.next = [];
	}

	/**
	 * @param {Node} node 
	 * @returns {Boolean}
	 */
	equals(node) {
		return this.move ? this.move==node.move:this._position==node._position
	}

	/**
	 * @returns {Position}
	 */
	get position() {
		return this._position ? new Position(this._position):this.prev.position.move(this.move)
	}

	/**
	 * @param {Position|String}
	 */
	set position(position) {
		this._position = position;
	}

	/**
	 * @returns {Node}
	 */
	get first() {
		return this.prev ? this.prev.first:this
	}

	/**
	 * @returns {Node}
	 */
	get last() {
		return this.next[0] ? this.next[0].last:this
	}

	/**
	 * @returns {Movelist}
	 */
	get movelist() {
		const movelist = new Movelist([new Node({id: 0, position: this.position.fen})]);
		(function loop(prev, nodes) {
			nodes.forEach(node => loop(movelist.save(Object.assign({}, node, { id: null, prev })), node.next) );
		})(movelist.first(), this.next);
		return movelist
	}

	/**
	 * @returns {Boolean}
	 */
	get mainline() {
		if(this.prev) return this.prev.next[0] == this ? this.prev.mainline:false
		return true
	}

	/**
	 * @returns {Boolean}
	 */
	get threefold() {
		let hashes = [];
		let position;
		(function loop(node) {
			if(!node.move) {
				position = node.position;
				hashes.push(position.key);
				return
			}
			loop(node.prev);
			position.move(node.move);
			hashes.push(position.key);
		})(this);
		const hash = hashes.pop();

		return hashes.reduce((n, _hash) => n+(hash==_hash), 0) >= 2
	}

	/**
	 * @returns {Number} status
	 */
	get status() {
		return this.position.status||(this.threefold ? chessalpha.status.THREEFOLD:(this.prev && this.prev.prev ? chessalpha.status.STARTED:0))
	}

	/**
	 * @param {Function} cb 
	 */
	remove(cb) {
		if(this.prev) this.prev.next.splice(this.prev.next.indexOf(this),1);
		let i = this.next.length;
		while(i--) this.next[i].remove(cb);
		if(cb) cb(this);
	}

	/**
	 * @returns {Object}
	 */
	toJSON() {
		const	json = {};
		const next = [];
		let	i = this.next.length;
		while(i--) next[i] = this.next[i].id;

		for(let key in this)
			if(typeof(this[key])!='function' && !~['id','next','prev','_position'].indexOf(key))
				json[key] = this[key];

		if(this._position) json.position = this._position;

		json.next = next;
		return json
	}
	
}

const RE = {
	TAG: new RegExp(/\[([^\]]*)\s"([^\]]*)"\]/),
	NAG: new RegExp(/^\$([\d]){1,3}/),
	STARTMOVE: new RegExp(/(^[\d]+)[\.](?!\.)/),
	BLACKMOVE: new RegExp(/(^[\d]+)[\.]{3}/),
	COMMENT: new RegExp(/^\{([^\}]*)\}/),
	COMMENTTAG: new RegExp(/\[\%(.*)\s(.*)\]/),
	ALTERNATE: new RegExp(/^\(([^\)]*)\)/),
	ALTERNATESTART: new RegExp(/(\()/),
	ALTERNATEEND: new RegExp(/(\))/),
	MOVE: new RegExp(/^(?:[KQRNBP]?[a-h]?[1-8]?x?@?-?[a-h][1-8]\=?[KQRNBP]?|O(-?O){1,2}|\-\-)[\+#]?(\s*[\!\?]+)?/),
	END: new RegExp(/(1\/2-1\/2|1-0|0-1|\*)/)
};

/**
 * Finds start and end of subline in pgn
 * 
 * @param {String}
 * @returns {String}
 */
function findMatchingParen(str) {
	let i = 0, chr, left = 0, right = 0;

	while (chr = str.charAt(i++)) {

		if (RE.ALTERNATESTART.test(chr)) {
			left++;
		}
		else if (RE.ALTERNATEEND.test(chr)) {
			right++;
		}

		if (left == right && left != 0) {
			return str.substring(1, i-1)
		}
	}

	return ''
}

class Game {

	/**
	 * 
	 * @param {String} pgn 
	 * @param {Object} options 
	 * @returns {Boolean}
	 */
	static parsePgn(pgn, options = {}) {
		let match, prev, created = false;

		this.__setPgnCommentAfter = false;
		this.__setPgnCommentBefore = '';

		while (true) {
			pgn = pgn.trim();

			// tag
			if (match = pgn.match(RE.TAG)) {
				if(!created) {
					if(options.onCreate) options.onCreate();
					created = true;
				}

				const name = match[1].toLowerCase();
				const	value  = match[2];

				if(name && value && options.onTag) options.onTag({name, value});
				else if(options.debug) console.error('Invalid PGN: Malformed tag');

				pgn = pgn.replace(match[0], '');
			}

			// start of a new move set
			else if (match = pgn.match(RE.STARTMOVE)) {
				if(options.onStartMove) options.onStartMove(match[0]);
				pgn = pgn.replace(match[0], '').trim();
			}

			// move
			else if (match = pgn.match(RE.MOVE)) {
				if(options.onMove) options.onMove(match[0]);
				pgn = pgn.replace(match[0], '').trim();
			}

			// black
			else if (match = pgn.match(RE.BLACKMOVE)) {
				if(options.onBlackMove) options.onBlackMove(match[0]);
				pgn = pgn.replace(match[0], '').trim();
			}

			// nag
			else if (match = pgn.match(RE.NAG)) {
				if(options.onNag) options.onNag(match[0]);
				pgn = pgn.replace(match[0], '').trim();
			}
			
			// comment
			else if (match = pgn.match(RE.COMMENT)) {
				let commentMatch;
				let comment = match[1];
				const originalComment = comment;
				const tags = {};
				while(true) {
					if(commentMatch = comment.match(RE.COMMENTTAG)) {
						tags[commentMatch[1].toLowerCase()] = commentMatch[2];
						comment = comment.replace(commentMatch[0], '').trim();
					}
					if (comment === prev) break
					if (comment.length < 1) break

					commentMatch = null;
					prev = comment;
				}
				
				if(options.onComment) options.onComment(comment, tags, originalComment);
				pgn = pgn.replace(match[0], '').trim();
			}

			// alternate line
			else if (match = pgn.match(RE.ALTERNATE)) {
				const altStr = findMatchingParen(pgn);
				if(options.onAlternate) options.onAlternate(altStr);
				pgn = pgn.replace('('+altStr+')', '').trim();
			}

			// game decision
			else if (match = pgn.match(RE.END)) {
				if(options.onDecision) options.onDecision(match[0]);
				pgn = pgn.replace(match[0], '').trim();
			}

			// nothing left to parse?
			if (pgn.length < 1) {
				if(options.onComplete) options.onComplete();
				if(options.debug) console.info('Parsing Complete!');
				break
			}

			// break out of loop if no rule matched
			if (pgn === prev) {
				if(options.debug) console.error('Invalid PGN: Unknown error', 'string: '+pgn);
				return false
			}

			match = null;
			prev = pgn;
		}

		return true
	}

	/**
	 * 
	 * @param {Game} game
	 * @returns {Object} 
	 */
	static pgnOptions(game) {
		const options = {
			onTag: ({name, value}) => {
				if(~["event","site"].indexOf(name)) {
					game[name] = value;
					return
				}
				if(name==="round") {
					const split = value.split(".");
					game.round = +split[0];
					if(split[1]) game.board = +split[1];
					return
				}
				if(name==="board") {
					game.board = +value;
					return
				}
				if(name==="date") {
					game.date = new Date(value);
					return
        }
				if(name=="fen") {
					game.initPosition([new Node({id: 0, position: value, start: true})]);
					return
				}

				if(~["white","black"].indexOf(name)) {
					let i = value.indexOf(",") + 1;
					if(i) value = value.substr(i).trim() + " " + value.substr(0, i - 1);
				}

				if(name.startsWith("white")) {
					game.white = game.white||{};
					game.white[name.substr(5)||"name"] = value;
				}
				else if(name.startsWith("black")) {
					game.black = game.black||{};
					game.black[name.substr(5)||"name"] = value;
				}

			},

			onMove: san => {
				if(!game.move(san)) throw new Error(game.position.ascii + "color: " + game.position.tomove + "|move not possible " + san + ", " + game.position.validMoves.map(move => game.position.moveToSan(move)))
				if(game.__setPgnCommentBefore) {
					game.current.textbefore = game.__setPgnCommentBefore;
					game.__setPgnCommentBefore = '';
				}
				game.__setPgnCommentAfter = true;
			},

			onNag: nag => {
				game.current.nags = game.current.nags||[];
				game.current.nags.push(nag);
			},

			onAlternate: str => {
				game.__setPgnCommentAfter = false;
				let c = game.current;
				let	p = new Position(game.position);

				game.current = game.current.prev;
				game.position = game.current.position;

				Game.parsePgn(str, options);

				game.current = c;
				game.position = p;
			},

			onComment: (comment, tags) => {
				comment = comment.replace(/(?:\r\n|\r|\n)/g, ' ');
				if(game.__setPgnCommentAfter) game.current.textafter = comment;
				else game.__setPgnCommentBefore = comment;
				game.__setPgnCommentAfter = false;
				
				if(tags) {
					if(tags.clk) {
						if(game.current.prev) {
							const color1 = chessalpha.formatColorstring(game.position.tomove) + "time";
							game.current[color1] = game.current.prev[color1];
						}
						game.current[chessalpha.formatColorstring(game.position.tomove^24) + "time"] = (([hours,minutes,seconds]) => { return ((+hours * 60 + +minutes) * 60 + +seconds) * 1000 })(tags.clk.split(':'));
					}
				}
			},

			onDecision: result => {
				game.result = chessalpha.formatResult(result);
				game.status = game.result ? chessalpha.status.UNKNOWN:game.current.start ? chessalpha.status.CREATED:chessalpha.status.STARTED;

				// node missing time
				game.movelist.list.forEach(node => {
					// TODO: if game is ongoing, calc time based on timestamp
					if(node.prev) {
						if(!node.whitetime) node.whitetime = node.prev.whitetime;
						if(!node.blacktime) node.blacktime = node.prev.blacktime;
					}
				});
			}
		};
		return options
	}

	/**
	 * 
	 * @param {Game|String} game 
	 */
	constructor(game) {
		if(typeof game=="string") {
			this.status = 0;
			this.result = 0;
			this.pgn = game;
		}
		else 
			this.init(game||{});
	}

	/**
	 * 
	 * @param {Game} game 
	 */
	init(game) {
		for(let key in game) this[key] = game[key];
		
		this.initPosition(game.movelist, game.current||0);

		this.white = this.white||{};
		this.black = this.black||{};
		
		this.result = game.result||0;
		this.status = game.status ? game.status:this._status();
	}

	/**
	 * 
	 * @param {Array} movelist 
	 * @param {Node} current 
	 */
	initPosition(movelist, current = 0) {
		this.movelist = new Movelist(movelist||[new Node({id: 0, position:'start', start: true})]);
		this.current = typeof current === "number" ? this.movelist.list[current]:current;
		this.position = this.current.position;

		if(this.timecontrol) Object.assign(this.current, this.movetime());
	}

	/**
	 * @returns {String}
	 */
	get pgn() {

		const result = ({"4":"1/2-1/2","8":"1-0","16":"0-1"})[this.result]||'*';
		let tags = "";
		let moves = "";
		let current = this.movelist.first();
		let	position = current.position;

		if(this.event) tags += `[Event "${this.event}"]\n`;
		if(this.site) tags += `[Site "${this.site}"]\n`;
		if(this.date) tags += `[Date "${this.date.getFullYear()}.${("0"+(this.date.getMonth()+1)).slice(-2)}.${("0"+this.date.getDate()).slice(-2)}"]\n`;
		if(this.round) tags += `[Round "${this.round}"]\n`;
		if(this.board) tags += `[Board "${this.board}"]\n`;
		tags += '[White "' + (this.white.name||'') + '"]\n';
		tags += '[Black "' + (this.black.name||'') + '"]\n';
		tags += '[Result "' + result + '"]\n';

		if(!position.hash.equals(new Position('start').hash)) tags += '[FEN "' + position.fen + '"]\n';

		/**
		 * 
		 * @param {Node} node 
		 * @param {Number} halfmove 
		 * @param {Boolean} showNumber 
		 * @returns {String}
		 */
		function move(node, halfmove, showNumber) {
			const isWhite = halfmove % 2;
			
			let ms = node[(isWhite ? "white":"black") + "time"];
			let time = "";
			if(ms) {
				ms = Math.floor(ms / 1000);
				const h = Math.floor(ms / 3600);
				const m = Math.floor((ms - (h * 3600)) / 60);
				const s = Math.floor(ms - (h * 3600) - (m * 60));
				time = '[%clk '+h+':'+('0'+m).slice(-2)+':'+('0'+s).slice(-2)+']';
			}
			let textafter = node.textafter ? time+' '+node.textafter.trim():time;
			return (node.textbefore ? "{" + node.textbefore.trim() + "} ":"") + 
				(showNumber || isWhite ? (~~(halfmove/2) + isWhite) + (isWhite ? ". ":"... "):'') + node.san + 
				(node.nags ? ' ' + node.nags.reduce((str, nag) => str + nag, ''):'') + " " +
				(textafter ? "{" + textafter + "} ":"")
		}

		(function loop(nodes, halfmove, showNumber) {
			let i=0;
			if(nodes[i]) {
				halfmove += 1;
				
				moves += move(nodes[i], halfmove, showNumber);
				while(++i<nodes.length) {
					moves += '(' + move(nodes[i], halfmove, nodes.length > 1);
					loop(nodes[i].next, halfmove, false);
					moves = moves.substr(0, moves.length - 1) + ') ';
				}
				loop(nodes[0].next, halfmove, nodes.length > 1);
			}
		})(current.next, position.halfmove, true);

		let lastBlankspace;
		for(let i=1;i<moves.length;i++) {
			if(moves.charAt(i) == " ") lastBlankspace = i;
			if(!(i%80)) moves = moves.substr(0,lastBlankspace) + '\n' + moves.substr(lastBlankspace + 1);
		}
		return tags + '\n' + moves + result
	}

	/**
	 * @param {String}
	 */
	set pgn(pgn) {
		/*
		 * VALIDTAGS
		 * ['event', 'site', 'date', 'round', 'white',
		 * 'black', 'result', 'eco', 'whitefideid', 'blackfideid', 'whitetitle', 'blacktitle', 'whiteelo', 'blackelo',
		 * 'plycount', 'eventdate', 'fen']
		 */

		if(!this.position || !this.current) this.initPosition();

		this.constructor.parsePgn(pgn, this.constructor.pgnOptions(this));
	}

	/**
	 * 
	 * @param {Number} move 
	 * @param {Array} validmoves 
	 * @param {Node} current 
	 * @param {Number} index 
	 * @returns {Game}
	 */
	move(move, validmoves = this.position.validMoves, current = this.current, index) {
		let san;

		if(+move!==move) {
			san = move;
			move = this.position.sanToMove(move);
		}

		if(~validmoves.indexOf(move)) {
			this.current = this.movelist.move(move, current, san||this.position.moveToSan(move), index);
			this.position.move(move);
			if(this.timecontrol) Object.assign(this.current, this.movetime());

			if(this.status < chessalpha.status.ABORTED) {
				this.status = this._status()||this.status; // this._status() returns 0 if not decided and movenr<2
				this.result = this._result()||this.result;
			}

			return this
		}

		return null
	}

	/**
	 * 
	 * @param {Node} node 
	 * @param {Number} halfmoveClockStart 
	 * @returns {Boolean}
	 */
	withinStartRange(node = this.current, halfmoveClockStart = this.halfmoveClockStart||3) {
		do { if(node.start) return true } 
		while(--halfmoveClockStart && (node = node.prev))
		return false
	}

	/**
	 * 
	 * @param {Array} timecontrol 
	 * @param {Number} timestamp 
	 * @param {Node} node 
	 * @param {Number} tomove 
	 * @param {Number} halfmoveClockStart 
	 * @returns {Object}
	 */
	movetime(timecontrol = this.timecontrol, timestamp = Date.now(), node = this.current, tomove = this.position.tomove, halfmoveClockStart = this.halfmoveClockStart||3) {
		const prev = node.prev;
		const increment = timecontrol[0].increment||0;
		let { whitetime, blacktime } = prev||{};

		if(this.withinStartRange(node, halfmoveClockStart)) {
			whitetime = prev&&prev.whitetime||node.whitetime||timecontrol[0].start;
			blacktime = prev&&prev.blacktime||node.blacktime||timecontrol[0].start;
		}
		else if(prev) {
      const time = prev.timestamp - timestamp + increment;
      if(tomove==chessalpha.WHITE) blacktime += time;
      else whitetime += time;
    }
    return { timestamp, whitetime, blacktime }
	}

	/**
	 * 
	 * @param {Number} moveid 
	 * @returns {Game}
	 */
	goto(moveid) {
		this.current = this.movelist.list[moveid];
		this.position = this.current.position;
		return this
  }
	
	/**
	 * 
	 * @param {Number} index 
	 * @returns {Game}
	 */
	next(index) {
		index=index||0;
		if(this.current.next[index]) {
			this.current = this.current.next[index];
			this.position = this.current.position;
		}
		return this
	}

	/**
	 * @returns {Game}
	 */
	prev() {
		if(this.current.prev) {
			this.current = this.current.prev;
      this.position = this.current.position;
		}
		return this
	}

	/**
	 * 
	 * @param {Node} current 
	 * @returns {Number}
	 */
	_status(current = this.current) {
		let status = current.status||this.result&&chessalpha.status.UNKNOWN||0;
		if(status < chessalpha.status.ABORTED && this.timecontrol) {
			if(this.current[chessalpha.formatColorstring(this.position.tomove) + 'time']<1) {
				status = this.position._pieceList(this.position.tomove^24)[0].length>1 ? chessalpha.status.OUTOFTIME:chessalpha.status.INSUFFICIENTMATERIAL;
			}
		}
		return status
	}

	/**
	 * 
	 * @param {Number} status 
	 * @param {Position} position 
	 * @returns {Number}
	 */
	_result(status = this.status, position = this.position) {
		if(status < chessalpha.status.MATE) return 0
		if(status >= chessalpha.status.UNKNOWN) return chessalpha.result.UNKNOWN
		if(status < chessalpha.status.DRAW) return position.tomove^24
		return chessalpha.result.DRAW
	}

	/**
	 * @returns {Object}
	 */
	toJSON() {
		const obj = {};
		for(let key in this) {
			if(this[key]!=null) {
				if(this[key].toJSON) obj[key] = this[key].toJSON();
				else obj[key] = this[key];
			}
		}
		obj.current = obj.current.id;
		return obj
	}

}

class Match {
  constructor(match) {
    match = match||{};

    for(var key in match) {
      this[key] = match[key];
    }
    
    this.home = match.home||"Hemmalag";
    this.away = match.away||"Bortalag";

    this.homePoints = match.homePoints||0;
    this.awayPoints = match.awayPoints||0;
    
    this.homeCaptains = match.homeCaptains||[];
    this.awayCaptains = match.awayCaptains||[];
    
    this.games = match.games||[];
  }
}

Object.assign(chessalpha, { Position, Node, Movelist, Game, Match });

module.exports = chessalpha;
