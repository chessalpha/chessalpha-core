class Match {
  constructor(match) {
    match = match||{}

    for(var key in match) {
      this[key] = match[key]
    }
    
    this.home = match.home||"Hemmalag"
    this.away = match.away||"Bortalag"

    this.homePoints = match.homePoints||0
    this.awayPoints = match.awayPoints||0
    
    this.homeCaptains = match.homeCaptains||[]
    this.awayCaptains = match.awayCaptains||[]
    
    this.games = match.games||[]
  }
}

export default Match