import chessalpha from './core'
import Position from './position'
import { Movelist, Node } from'./movelist.js'
import Game from'./game.js'
import Match from'./match.js'

Object.assign(chessalpha, { Position, Node, Movelist, Game, Match })

export default chessalpha