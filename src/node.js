import chessalpha from './core.js'
import Movelist from './movelist.js'
import Position from './position.js'

class Node {

	/**
	 * @param {Node} node 
	 */
	constructor(node) {
		Object.assign(this,node)
		this.next = []
	}

	/**
	 * @param {Node} node 
	 * @returns {Boolean}
	 */
	equals(node) {
		return this.move ? this.move==node.move:this._position==node._position
	}

	/**
	 * @returns {Position}
	 */
	get position() {
		return this._position ? new Position(this._position):this.prev.position.move(this.move)
	}

	/**
	 * @param {Position|String}
	 */
	set position(position) {
		this._position = position
	}

	/**
	 * @returns {Node}
	 */
	get first() {
		return this.prev ? this.prev.first:this
	}

	/**
	 * @returns {Node}
	 */
	get last() {
		return this.next[0] ? this.next[0].last:this
	}

	/**
	 * @returns {Movelist}
	 */
	get movelist() {
		const movelist = new Movelist([new Node({id: 0, position: this.position.fen})]);
		(function loop(prev, nodes) {
			nodes.forEach(node => loop(movelist.save(Object.assign({}, node, { id: null, prev })), node.next) )
		})(movelist.first(), this.next)
		return movelist
	}

	/**
	 * @returns {Boolean}
	 */
	get mainline() {
		if(this.prev) return this.prev.next[0] == this ? this.prev.mainline:false
		return true
	}

	/**
	 * @returns {Boolean}
	 */
	get threefold() {
		let hashes = []
		let position
		(function loop(node) {
			if(!node.move) {
				position = node.position
				hashes.push(position.key)
				return
			}
			loop(node.prev)
			position.move(node.move)
			hashes.push(position.key)
		})(this)
		const hash = hashes.pop()

		return hashes.reduce((n, _hash) => n+(hash==_hash), 0) >= 2
	}

	/**
	 * @returns {Number} status
	 */
	get status() {
		return this.position.status||(this.threefold ? chessalpha.status.THREEFOLD:(this.prev && this.prev.prev ? chessalpha.status.STARTED:0))
	}

	/**
	 * @param {Function} cb 
	 */
	remove(cb) {
		if(this.prev) this.prev.next.splice(this.prev.next.indexOf(this),1)
		let i = this.next.length
		while(i--) this.next[i].remove(cb)
		if(cb) cb(this)
	}

	/**
	 * @returns {Object}
	 */
	toJSON() {
		const	json = {}
		const next = []
		let	i = this.next.length
		while(i--) next[i] = this.next[i].id

		for(let key in this)
			if(typeof(this[key])!='function' && !~['id','next','prev','_position'].indexOf(key))
				json[key] = this[key]

		if(this._position) json.position = this._position

		json.next = next
		return json
	}
	
}

export default Node