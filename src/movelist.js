import chessalpha from './core.js'
import Position from './position.js'

class Movelist {

	/**
	 * 
	 * @param {Array|Movelist} movelist 
	 */
	constructor(movelist) {
		this.list = []

		if(movelist) {
			//problem om endast 1 ställning
			if(movelist[0] && movelist[0].next && typeof movelist[0].next[0] === 'number') this.toPointers(movelist,0)
			else this.list = (movelist.list||movelist).map(node => new Node(node))
		}
	}

	/**
	 * 
	 * @param {Array} list 
	 * @param {Node} node 
	 * @param {Node} prev
	 * @returns {Node} 
	 */
	toPointers(list, node, prev) {
		if(typeof node === 'number') node = list[node]
		let next = node.next||[]
		node.id = this.list.length
		if(node.prev == null && prev) node.prev = prev
		node = this.save(node)
		for(let i=0; i<next.length; i++) this.toPointers(list, next[i], node)
		return node
	}

	/**
	 * 
	 * @param {Node|Object} object 
	 * @param {Number} index 
	 * @returns {Node}
	 */
	save(object, index) {
		object.id = object.id||this.list.length
		const node = new Node(object)
		
		if(object.prev && object.prev.next)
			object.prev.next.splice(index!=null ? index:object.prev.next.length, 0, node)
		this.list[node.id] = node
		return node
	}

	/**
	 * 
	 * @param {Number} move 
	 * @param {Node} prev 
	 * @param {String} san 
	 * @param {Number} index 
	 * @returns {Node}
	 */
	move(move, prev, san, index) {
		if(index==null) {
			let next = prev.next
			let	i = next.length
			while(i--) if(move === next[i].move) return next[i]
		}
		return this.save({move, prev, san, timestamp: Date.now()}, index)
	}

	/**
	 * @deprecated
	 * @param {Node} node 
	 * @returns {Position}
	 */
	position(node) {
		console.warn("Deprecated, use Node.position instead of Movelist.position")
		return node.position
	}

	/**
	 * 
	 * @param {Node} node 
	 * @returns {Node}
	 */
	first(node) {
		return node ? node.first:this.list[0]
	}

	/**
	 * 
	 * @param {Node} node 
	 * @returns {Node}
	 */
	last(node) {
		return (node||this.list[0]).last
	}

	/**
	 * 
	 * @param {Node|Number} node 
	 */
	remove(node) {
		if(typeof node == 'number') node = this.list[node]
		node.remove(node => {
			delete this.list[node.id]
		})
	}

	/**
	 * 
	 * @param {Node} node1 
	 * @param {Node} node2 
	 * @param {Boolean} prepend 
	 * @returns {Movelist}
	 */
	_merge(node1, node2, prepend) {
		const nodesToAdd = []
		for(let i=0;i<node2.next.length;i++) {
			for(let j=0;j<node1.next.length;j++) {
				if(node1.next[j].equals(node2.next[i])) this._merge(node1.next[i],node2.next[i],prepend)
				else nodesToAdd.push(node2.next[i])
			}
		}

		for(let i=0;i<nodesToAdd.length;i++) {
			nodesToAdd[i].id = null
			this.save(nodesToAdd[i])
		}

		node1.next = prepend ? nodesToAdd.concat(node1.next):node1.next.concat(nodesToAdd)

		return this
	}

	/**
	 * 
	 * @param {Node} node1 
	 * @param {Node} node2 
	 * @param {Boolean} prepend 
	 * @returns {Movelist}
	 */
	merge(node1, node2, prepend) {
		return node1.position.hash.equals(node2.position.hash) ? this._merge(node1, node2, prepend):null
	}

	/**
	 * @returns {Object}
	 */
	toJSON() {
		return this.list.map(node => node.toJSON())
	}
}


class Node {

	/**
	 * @param {Node} node 
	 */
	constructor(node) {
		Object.assign(this,node)
		this.next = []
	}

	/**
	 * @param {Node} node 
	 * @returns {Boolean}
	 */
	equals(node) {
		return this.move ? this.move==node.move:this._position==node._position
	}

	/**
	 * @returns {Position}
	 */
	get position() {
		return this._position ? new Position(this._position):this.prev.position.move(this.move)
	}

	/**
	 * @param {Position|String}
	 */
	set position(position) {
		this._position = position
	}

	/**
	 * @returns {Node}
	 */
	get first() {
		return this.prev ? this.prev.first:this
	}

	/**
	 * @returns {Node}
	 */
	get last() {
		return this.next[0] ? this.next[0].last:this
	}

	/**
	 * @returns {Movelist}
	 */
	get movelist() {
		const movelist = new Movelist([new Node({id: 0, position: this.position.fen})]);
		(function loop(prev, nodes) {
			nodes.forEach(node => loop(movelist.save(Object.assign({}, node, { id: null, prev })), node.next) )
		})(movelist.first(), this.next)
		return movelist
	}

	/**
	 * @returns {Boolean}
	 */
	get mainline() {
		if(this.prev) return this.prev.next[0] == this ? this.prev.mainline:false
		return true
	}

	/**
	 * @returns {Boolean}
	 */
	get threefold() {
		let hashes = []
		let position
		(function loop(node) {
			if(!node.move) {
				position = node.position
				hashes.push(position.key)
				return
			}
			loop(node.prev)
			position.move(node.move)
			hashes.push(position.key)
		})(this)
		const hash = hashes.pop()

		return hashes.reduce((n, _hash) => n+(hash==_hash), 0) >= 2
	}

	/**
	 * @returns {Number} status
	 */
	get status() {
		return this.position.status||(this.threefold ? chessalpha.status.THREEFOLD:(this.prev && this.prev.prev ? chessalpha.status.STARTED:0))
	}

	/**
	 * @param {Function} cb 
	 */
	remove(cb) {
		if(this.prev) this.prev.next.splice(this.prev.next.indexOf(this),1)
		let i = this.next.length
		while(i--) this.next[i].remove(cb)
		if(cb) cb(this)
	}

	/**
	 * @returns {Object}
	 */
	toJSON() {
		const	json = {}
		const next = []
		let	i = this.next.length
		while(i--) next[i] = this.next[i].id

		for(let key in this)
			if(typeof(this[key])!='function' && !~['id','next','prev','_position'].indexOf(key))
				json[key] = this[key]

		if(this._position) json.position = this._position

		json.next = next
		return json
	}
	
}

export { Movelist, Node }