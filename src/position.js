import chessalpha from './core'

const BOARD = new Uint8Array([
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128])

const STARTBOARD = new Uint8Array([
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128,  20,  18,  19,  21,  22,  19,  18,  20, 128, 128, 128, 128,
128, 128, 128, 128,  17,  17,  17,  17,  17,  17,  17,  17, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   0,   0,   0,   0,   0,   0,   0,   0, 128, 128, 128, 128,
128, 128, 128, 128,   9,   9,   9,   9,   9,   9,   9,   9, 128, 128, 128, 128,
128, 128, 128, 128,  12,  10,  11,  13,  14,  11,  10,  12, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128,
128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128, 128])

const crMask = new Uint8Array([
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 7,15,15,15, 3,15,15,11, 0, 0, 0, 0,
0, 0, 0, 0,15,15,15,15,15,15,15,15, 0, 0, 0, 0,
0, 0, 0, 0,15,15,15,15,15,15,15,15, 0, 0, 0, 0,
0, 0, 0, 0,15,15,15,15,15,15,15,15, 0, 0, 0, 0,
0, 0, 0, 0,15,15,15,15,15,15,15,15, 0, 0, 0, 0,
0, 0, 0, 0,15,15,15,15,15,15,15,15, 0, 0, 0, 0,
0, 0, 0, 0,15,15,15,15,15,15,15,15, 0, 0, 0, 0,
0, 0, 0, 0,13,15,15,15,12,15,15,14, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0])


class Position {

  /***
   * Constructs position from string (empty or start), fen string or clones a Position
   * 
   * @param {String|Position}
   */
	constructor(position = "start") {
		if(position=="empty") this.position = { board: BOARD, cr: 0 }
		else if(position=="start") this.position = { board: STARTBOARD }
		else if(typeof position=="string") this.fen = position
		else if(typeof position=="object") this.position = position
		else throw new Error("Position constructor error")
	}

	/**
	 * @param {Position}
	 */
	set position(position) {
		this.board = (position.board||STARTBOARD).slice()
		this.eps = position.eps||-1
		this.cr = position.cr==null ? 15:position.cr
		this.tomove = position.tomove||chessalpha.WHITE
		this.halfmove = position.halfmove||0
		this.move50 = position.move50||0
		this.hash = position.hash||this.generateHash()
	}

	/***
	 * @return {String} fen string
	 */
	get fen() {
		const board = this.board
		const pieceArray = ["p","n","b","r","q","k"]

		let fen = ""
		let empty = 0
		
		for(let i=0; i<64; i++) {
			let square = chessalpha.format64To192(i)
			if (!board[square]) {
        empty++
      }
			else {
				if (empty > 0) {
					fen += empty
					empty = 0
				}
				let piece = pieceArray[(board[square]&7)-1]
				fen += ((board[square]&24) === chessalpha.WHITE) ? piece.toUpperCase() : piece.toLowerCase()
			}

			if(!((i + 1) & 7)) {
				if (empty > 0) {
					fen += empty
				}
				if(i != 63) {
          fen += "/"
        }
				empty = 0
			}
		}

		fen += this.tomove&chessalpha.WHITE ? " w ":" b "

		if(!this.cr) fen+="-"
		else {
			if(this.cr&1) fen+="K"
			if(this.cr&2) fen+="Q"
			if(this.cr&4) fen+="k"
			if(this.cr&8) fen+="q"
		}
		fen += " "

		fen += this.eps == -1 ? "-":chessalpha.formatSan(this.eps)
		fen += " "

		fen += this.move50 + " "
		
		fen += ~~(this.halfmove/2) + 1

		return fen
	}

	/**
	 * @param {String}
	 */
	set fen(fen) {
		
		this.board = BOARD.slice()

		const chunks = fen.split(" ")
		let	square = 36
		let pieces = chunks[0]
		let validFen = false
		let i = 0
		let l = pieces.length
		
		for(i;i<l;i++) {
			const c = pieces.charAt(i)

			if(c=="/") {
				square+=8
				validFen = true
			}

			else {
				if (c >= "0" && c <= "9")
					square+=parseInt(c)
				else {
					let piece = c >= "a" && c <= "z" ? chessalpha.BLACK:chessalpha.WHITE
					piece |= chessalpha.formatCharToPiece(c.toLowerCase())
					
					if((piece&24)!=piece) this.board[square] = piece
					square++
				}
			}
		}

		if(!validFen) throw new Error("Illegal fen")

		this.tomove = chunks[1].charAt(0) == "b" ? chessalpha.BLACK:chessalpha.WHITE
		
		this.cr = 0
		if(chunks[2]) {
			if(~chunks[2].indexOf("K")) this.cr |= 1
			if(~chunks[2].indexOf("Q")) this.cr |= 2
			if(~chunks[2].indexOf("k")) this.cr |= 4
			if(~chunks[2].indexOf("q")) this.cr |= 8
		}
		
		this.eps = -1
		if (chunks[3] && chunks[3].indexOf("-") == -1) this.eps = chessalpha.formatSquare(chunks[3])

		this.move50 = +chunks[4]||0

		this.halfmove = (chunks[5]-1)*2
		if(this.tomove == chessalpha.BLACK) this.halfmove++

		this.hash = this.generateHash()
	}

	/**
	 * @returns {Array}
	 */
	get pieceList() {
		return this._pieceList()
	}

	/**
	 * @param {Number} color
	 * @returns {Array}
	 */
	_pieceList(color = 24) {
		const pieceList = [[],,,,,,,,,[],[],[],[],[],[],,,[],[],[],[],[],[]]
		const board = this.board
		let i=192
		while(i--) {
			if(board[i]&color) {
				pieceList[0].push(board[i])
				pieceList[board[i]].push(i)
			}
		}
		return pieceList
	}

	/**
	 * 
	 * @param {Number} square
	 * @returns {Array} 
	 */
	pieceListFrom(square) {
		const pieceList = [[],,,,,,,,,[],[],[],[],[],[],,,[],[],[],[],[],[]]
		const board = this.board
		if(board[square]&24) {
			pieceList[0].push(board[square])
			pieceList[board[square]].push(square)
		}
		return pieceList
	}

	/**
	 * @param {Number}
	 * @param {Number}
	 * @param {Number} flag
	 * @returns {Number} move
	 */
	createMove(from, to, promote) {
		if(!to) [from, to, promote] = this._splitMove(from)

		if(from<36 || to<36) return from|to<<8
		const board = this.board, piece = board[from]
		let flags = 0

		// castle
		if((piece&7)==chessalpha.KING && (from==40 || from==152)) {
			if(to-from==2 && (board[to+1]&7)==chessalpha.ROOK && !board[to] && !board[from+1]) flags |= chessalpha.CastleKing
			else if(from-to==2 && (board[to-2]&7)==chessalpha.ROOK && !board[to-1] && !board[to] && !board[from-1]) flags |= chessalpha.CastleQueen
		}

		// en passant
		if((piece&7)==chessalpha.PAWN && board[(to&0xF)|(from&0xF0)]==(piece^24) && board[to]==chessalpha.EMPTY) flags |= chessalpha.EPC

		// promote
		if(promote) flags|=promote
		else if((piece&7)==chessalpha.PAWN && ((to&0xF0)==32 || (to&0xF0)==144)) flags |= chessalpha.PromoteQueen

		return this._generateMove(from,to,flags)
	}

	/**
	 * @param {Number} square192
	 * @param {Number} square192
	 * @param {Number} flag
	 * @returns {Number} move
	 */
	_generateMove(from, to, flags = 0) {
		return from | (to << 8) | (flags << 16)
	}

	_splitMove(move) {
		return [move & 0xFF, (move >> 8) & 0xFF, move >> 16]
	}

	get validMoves() {
		return this._validMoves()
	}

	/**
	 * @param {Number} square192
	 * @param {Number} 
	 * @param {Number}
	 * @param {Number} square192
	 */
	_validMoves(from, tomove, cr, eps) {
		return this._allMoves(from, undefined, undefined, tomove, cr, undefined, eps).filter(move => {
      const position = new Position(this)
			if((chessalpha.CastleKing|chessalpha.CastleQueen)&(move >> 16)) {
				let square = move & 0xFF
				const oppositemoves = position._quiteMoves(undefined, undefined, undefined, position.tomove^24)
				const to = (move >> 8) & 0xFF
        const increment = chessalpha.CastleKing&(move >> 16) ? 1:-1
				while((square+=increment)<to) {
					if(oppositemoves.some(move => ((move>>8)&0xFF)==square)) return false
        }
			}
			position.move(move)
			return !position._inCheck(position.tomove^24)
		})
	}

	/**
	 * @returns {Array}
	 */
	get allMoves() {
		return this._allMoves()
	}

	/**
	 * @param {Number} square192
	 * @param {Array}
	 * @param {Array}
	 * @param {Array}
	 * @param {Number}
	 * @param {Number}
	 * @param {Boolean}
 	 * @param {Number} square192
	 * @returns {Array}
	 */
	_allMoves(from, moveStack = [], pieceList, board = this.board, tomove = this.tomove, cr = this.cr, inCheck = this.inCheck, eps = this.eps) {
		pieceList = pieceList||from ? this.pieceListFrom(from):this.pieceList
		this._quiteMoves(moveStack, pieceList, this.board, tomove, cr, inCheck)
		this._captureMoves(moveStack, pieceList, this.board, tomove, eps)
		return moveStack
	}

	get quiteMoves() {
		return this._quiteMoves()
	}

	/**
	 * @param {Array}
	 * @param {Array}
	 * @param {Array}
	 * @param {Number}
	 * @param {Number}
	 * @param {Boolean}
	 * @returns {Array}
	 */
	_quiteMoves(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove, cr = this.cr, inCheck = this.inCheck) {
		this._quiteMovesPawn(moveStack, pieceList, board, tomove)
		this._quiteMovesKnight(moveStack, pieceList, board, tomove)
		this._quiteMovesBishop(moveStack, pieceList, board, tomove)
		this._quiteMovesRook(moveStack, pieceList, board, tomove)
		this._quiteMovesQueen(moveStack, pieceList, board, tomove)
		this._quiteMovesKing(moveStack, pieceList, board, tomove, cr, inCheck)

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Uint8Array} board 
	 * @param {Number} tomove 
	 * @return {Array}
	 */
	_quiteMovesPawn(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove) {
		const inc = tomove == chessalpha.WHITE ? -16 : 16

		pieceList[chessalpha.PAWN|tomove].forEach(from => {
			let to = from + inc
			if(board[to]==0) {
				this._movePawnTo(moveStack,from,to)

				if(((from & 0xF0) == 0x30 && tomove != chessalpha.WHITE) || ((from & 0xF0) == 0x80 && tomove == chessalpha.WHITE)) {
					to += inc
					if (board[to]==0) {
						moveStack[moveStack.length] = this._generateMove(from, to)
					}
				}
			}
		})

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @return {Array}
	 */
	_quiteMovesKnight(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove) {
		let to

		pieceList[chessalpha.KNIGHT|tomove].forEach(from => {
			to = from + 31; if(board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from + 33; if(board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from + 14; if(board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from - 14; if(board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from - 31; if(board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from - 33; if(board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from + 18; if(board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from - 18; if(board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to)
		})

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @return {Array}
	 */
	_quiteMovesBishop(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove) {
		let to

		pieceList[chessalpha.BISHOP|tomove].forEach(from => {
			to = from - 15; while (board[to] == 0) { moveStack[moveStack.length] = this._generateMove(from, to); to -= 15 }
			to = from - 17; while (board[to] == 0) { moveStack[moveStack.length] = this._generateMove(from, to); to -= 17 }
			to = from + 15; while (board[to] == 0) { moveStack[moveStack.length] = this._generateMove(from, to); to += 15 }
			to = from + 17; while (board[to] == 0) { moveStack[moveStack.length] = this._generateMove(from, to); to += 17 }
		})

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @return {Array}
	 */
	_quiteMovesRook(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove) {
		let to

		pieceList[chessalpha.ROOK|tomove].forEach(from => {
			to = from - 1; while (board[to] == 0) { moveStack[moveStack.length] = this._generateMove(from, to); to-- }
			to = from + 1; while (board[to] == 0) { moveStack[moveStack.length] = this._generateMove(from, to); to++ }
			to = from + 16; while (board[to] == 0) { moveStack[moveStack.length] = this._generateMove(from, to); to += 16 }
			to = from - 16; while (board[to] == 0) { moveStack[moveStack.length] = this._generateMove(from, to); to -= 16 }
		})

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @return {Array}
	 */
	_quiteMovesQueen(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove) {
		const localPieceList = []
		const	queenList = pieceList[chessalpha.QUEEN|tomove]
		localPieceList[chessalpha.BISHOP|tomove] = queenList
		localPieceList[chessalpha.ROOK|tomove] = queenList
		this._quiteMovesBishop(moveStack, localPieceList, board, tomove)
		this._quiteMovesRook(moveStack, localPieceList, board, tomove)
		
		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @param {Number} cr
	 * @param {Boolean} inCheck
	 * @return {Array}
	 */
	_quiteMovesKing(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove, cr = this.cr, inCheck = this.inCheck) {
		let to

		pieceList[chessalpha.KING|tomove].forEach(from => {
			to = from - 15; if (board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from - 17; if (board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from + 15; if (board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from + 17; if (board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from - 1; if (board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from + 1; if (board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from - 16; if (board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from + 16; if (board[to] == 0) moveStack[moveStack.length] = this._generateMove(from, to)

			if(!inCheck) {
        if (tomove==chessalpha.BLACK) cr >>= 2

        if (cr & 1) {
					// Kingside castle
					if (board[from + 1] == chessalpha.EMPTY && board[from + 2] == chessalpha.EMPTY) {
						moveStack[moveStack.length] = this._generateMove(from, from + 0x02, chessalpha.CastleKing)
					}
				}
				if (cr & 2) {
          // Queenside castle
					if (board[from - 1] == chessalpha.EMPTY && board[from - 2] == chessalpha.EMPTY && board[from - 3] == chessalpha.EMPTY) {
						moveStack[moveStack.length] = this._generateMove(from, from - 0x02, chessalpha.CastleQueen)
					}
				}
			}

		})

		return moveStack
	}

	/**
	 * @returns {Array}
	 */
	get captureMoves() {
		return this._captureMoves()
	}

	/**
	 * @param {Array}
	 * @param {Array}
	 * @param {Array}
	 * @param {Number}
	 * @param {Number} square192
	 * @returns {Array}
	 */
	_captureMoves(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove, eps = this.eps) {			
		const enemy = tomove^24

		this._captureMovesPawn(moveStack, pieceList, board, tomove, enemy, eps)
		this._captureMovesKnight(moveStack, pieceList, board, tomove, enemy)
		this._captureMovesBishop(moveStack, pieceList, board, tomove, enemy)
		this._captureMovesRook(moveStack, pieceList, board, tomove, enemy)
		this._captureMovesQueen(moveStack, pieceList, board, tomove, enemy)
		this._captureMovesKing(moveStack, pieceList, board, tomove, enemy)

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @param {Number} enemy 
	 * @param {Number} eps 
	 * @returns {Array}
	 */
	_captureMovesPawn(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove, enemy = tomove^24, eps = this.eps) {
		let inc = tomove == chessalpha.WHITE ? -16 : 16
		let to

		pieceList[chessalpha.PAWN|tomove].forEach(from => {
			to = from + inc - 1
			if (board[to] & enemy) {
				this._movePawnTo(moveStack, from, to)
			}

			to = from + inc + 1
			if (board[to] & enemy) {
				this._movePawnTo(moveStack, from, to)
			}
		})

		if (eps != -1) {
			if(board[eps-inc+1]==(chessalpha.PAWN|tomove)) moveStack[moveStack.length] = this._generateMove(eps-inc+1, eps, chessalpha.EPC)
			if(board[eps-inc-1]==(chessalpha.PAWN|tomove)) moveStack[moveStack.length] = this._generateMove(eps-inc-1, eps, chessalpha.EPC)
		}

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @param {Number} enemy 
	 * @returns {Array}
	 */
	_captureMovesKnight(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove, enemy = tomove^24) {
		let to

		pieceList[chessalpha.KNIGHT|tomove].forEach(from => {
			to = from + 31; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from + 33; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from + 14; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from - 14; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from - 31; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from - 33; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from + 18; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from - 18; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
		})

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @param {Number} enemy 
	 * @returns {Array}
	 */
	_captureMovesBishop(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove, enemy = tomove^24) {
		let to

		pieceList[chessalpha.BISHOP|tomove].forEach(from => {
			to = from; do { to -= 15; } while (board[to] == 0); if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from; do { to -= 17; } while (board[to] == 0); if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from; do { to += 15; } while (board[to] == 0); if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from; do { to += 17; } while (board[to] == 0); if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
		})

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @param {Number} enemy 
	 * @return {Array}
	 */
	_captureMovesRook(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove, enemy = tomove^24) {
		let to

		pieceList[chessalpha.ROOK|tomove].forEach(from => {
			to = from; do { to--; } while (board[to] == 0); if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from; do { to++; } while (board[to] == 0); if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from; do { to -= 16; } while (board[to] == 0); if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from; do { to += 16; } while (board[to] == 0); if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
		})

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @param {Number} enemy 
	 * @returns {Array}
	 */
	_captureMovesQueen(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove, enemy = tomove^24) {
		const localPieceList = []
		const	queenList = pieceList[chessalpha.QUEEN|tomove]
		localPieceList[chessalpha.BISHOP|tomove] = queenList
		localPieceList[chessalpha.ROOK|tomove] = queenList
		this._captureMovesBishop(moveStack, localPieceList, board, tomove, enemy)
		this._captureMovesRook(moveStack, localPieceList, board, tomove, enemy)
		
		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Array} pieceList 
	 * @param {Array} board 
	 * @param {Number} tomove 
	 * @param {Number} enemy 
	 * @returns {Array}
	 */
	_captureMovesKing(moveStack = [], pieceList = this.pieceList, board = this.board, tomove = this.tomove, enemy = tomove^24) {
		let to
	
		pieceList[chessalpha.KING|tomove].forEach(from => {
			to = from - 15; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from - 17; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from + 15; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from + 17; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from - 1; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from + 1; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from - 16; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
			to = from + 16; if (board[to] & enemy) moveStack[moveStack.length] = this._generateMove(from, to)
		})

		return moveStack
	}

	/**
	 * 
	 * @param {Array} moveStack 
	 * @param {Number} from 
	 * @param {Number} to 
	 */
	_movePawnTo(moveStack, from, to) {
		const row = to&0xF0
		if(row==0x90 || row==0x20) {
			moveStack[moveStack.length] = this._generateMove(from, to, chessalpha.PromoteQueen)
			moveStack[moveStack.length] = this._generateMove(from, to, chessalpha.PromoteRook)
			moveStack[moveStack.length] = this._generateMove(from, to, chessalpha.PromoteBishop)
			moveStack[moveStack.length] = this._generateMove(from, to, chessalpha.PromoteKnight)
		}
		else moveStack[moveStack.length] = this._generateMove(from, to)
	}

	/**
	 * @return {Boolean}
	 */
	get inCheck() {
		return this._inCheck()
	}

	/**
	 * @param {Number} 
	 * @param {Array}
	 * @returns {Boolean}
	 */
	_inCheck(tomove = this.tomove, oppositemoves = this._captureMoves(undefined, undefined, undefined, tomove^24)) {
		const king = this.board.indexOf(chessalpha.KING|tomove)
		return oppositemoves.some(move => { return (move>>8&0xFF)==king })
	}

	/**
	 * @returns {Boolean}
	 */
	get checkmate() {
		return this._checkmate()
	}

	/**
	 * @param {Number} 
	 * @param {Array}
	 * @returns {Boolean}
	 */
	_checkmate(validmoves = this.validMoves, oppositemoves) {
		return !validmoves.length && this._inCheck(undefined, oppositemoves)
	}

	/**
	 * @returns {Boolean}
	 */
	get stalemate() {
		return this._stalemate()
	}

	/**
	 * @param {Number} 
	 * @param {Array}
	 * @returns {Boolean}
	 */
	_stalemate(validmoves = this.validMoves, oppositemoves) {
		return !validmoves.length && !this._inCheck(undefined, oppositemoves)
	}

	/**
	 * @returns {Boolean}
	 */
	get insufficientMaterial() {
		return this._insufficientMaterial()
	}

	/**
	 * 
	 * @param {Array} pieceList 
	 * @returns {Boolean}
	 */
	_insufficientMaterial(pieceList = this.pieceList) {
		const	n_pieces = pieceList[0].length
		const n_bishops = pieceList[chessalpha.BISHOP|chessalpha.WHITE].length + pieceList[chessalpha.BISHOP|chessalpha.BLACK].length
		const n_knights = pieceList[chessalpha.KNIGHT|chessalpha.WHITE].length + pieceList[chessalpha.KNIGHT|chessalpha.BLACK].length

		// k vs k
		if(n_pieces==2) return true

		// k vs kn ... or ... k vs kb
		else if (n_pieces === 3 && (n_bishops === 1 || n_knights === 1)) return true

		// kb vs kb where bishops are on the same color
		else if (n_pieces === 4 && n_bishops === 2) {
			const whiteSquare = pieceList[chessalpha.BISHOP|chessalpha.WHITE][0]
			const	blackSquare = pieceList[chessalpha.BISHOP|chessalpha.BLACK][0]
			if(whiteSquare && blackSquare) return ((whiteSquare&16)>>4)==(whiteSquare&1) === ((blackSquare&16)>>4)==(blackSquare&1)
		}

		return false
	}

	/**
	 * @returns {Number}
	 */
	get status() {
		return this._status()
	}

	/**
	 * 
	 * @param {Array} validmoves 
	 * @param {Array} oppositemoves 
	 * @returns {Number}
	 */
	_status(validmoves = this.validMoves, oppositemoves = this._captureMoves(undefined, undefined, undefined, this.tomove^24)) {
		return this._stalemate(validmoves, oppositemoves) ? chessalpha.status.STALEMATE:
			this.insufficientMaterial ? chessalpha.status.INSUFFICIENTMATERIAL:
			this._checkmate(validmoves, oppositemoves) ? chessalpha.status.MATE:
			this.move50 > 100 ? chessalpha.status.MOVE50:0
	}

	/**
	 * 
	 * @param {Number|String} move 
	 * @returns {Position}
	 */
	move(move) {
		if(typeof(move) === 'string') move = this.sanToMove(move)

		this.halfmove += 1
		this.move50 += 1

		if(move===1) return this

		this.hash.update(12 * 64 + 1)
		if(this.tomove!=24) this.tomove ^= 24

		if(move===0) return this
		
		const [ from, to, flags ] = this._splitMove(move)
		const board = this.board
		const from64 = chessalpha.format192To64(from)
		const to64 = chessalpha.format192To64(to)

		// insert piece, from = piece
		if(from<36) {
			if(board[to]) this.hash.update(chessalpha.formatPieceToHash(board[to])*64+to64)
			this.hash.update(chessalpha.formatPieceToHash(from)*64+to64)

			board[to] = from

			this.move50 = 0
			return this
		}

		// remove piece from square, not needed due to insert piece 0?
		else if(to<36) {
			if(board[from]) this.hash.update(chessalpha.formatPieceToHash(board[from])*64+to64)

			board[from] = chessalpha.EMPTY

			this.move50 = 0
			return this
		}

		let piece = board[from]
		const color = piece&24

		if(flags) {
			if(flags&chessalpha.CastleKing) {
				this.hash.update(chessalpha.formatPieceToHash(board[to+1])*64+to64-1)
				this.hash.update(chessalpha.formatPieceToHash(board[to+1])*64+to64+1)

				board[to-1] = board[to+1]
				board[to+1] = chessalpha.EMPTY
			}
			if(flags&chessalpha.CastleQueen) {
				this.hash.update(chessalpha.formatPieceToHash(board[to-2])*64+to64+1)
				this.hash.update(chessalpha.formatPieceToHash(board[to-2])*64+to64-2)

				board[to+1] = board[to-2]
				board[to-2] = chessalpha.EMPTY
			}
			if(flags&chessalpha.EPC) {
				const square = from&0xF0|to&0xF
				this.hash.update(chessalpha.formatPieceToHash(board[square])*64+chessalpha.format192To64(square))
				board[square] = chessalpha.EMPTY
			}
			if(flags&chessalpha.Promotion) {
				if(flags&chessalpha.PromoteQueen) piece = color|chessalpha.QUEEN
				else if(flags&chessalpha.PromoteBishop) piece = color|chessalpha.BISHOP
				else if(flags&chessalpha.PromoteKnight) piece = color|chessalpha.KNIGHT
				else piece = color|chessalpha.ROOK
			}
			this.move50 = 0
		}

		if(board[to]||(piece&7)==chessalpha.PAWN) this.move50 = 0

		this.hash.update(chessalpha.formatPieceToHash(board[from])*64+from64)
		this.hash.update(chessalpha.formatPieceToHash(piece)*64+to64)

		board[to] = piece
		board[from] = chessalpha.EMPTY

		if(~this.eps) this.hash.update(12 * 64 + 1 + 4 + this.eps&0xF - 4)
		this.eps = (piece&7)==chessalpha.PAWN && Math.abs(from-to)==32 ? (from+to)/2:-1
		if(~this.eps) this.hash.update(12 * 64 + 1 + 4 + this.eps&0xF - 4)

		if(this.cr&1) this.hash.update(12 * 64 + 1 + 1)
		if(this.cr&2) this.hash.update(12 * 64 + 1 + 2)
		if(this.cr&4) this.hash.update(12 * 64 + 1 + 3)
		if(this.cr&8) this.hash.update(12 * 64 + 1 + 4)
		this.cr &= crMask[from] & crMask[to]
		if(this.cr&1) this.hash.update(12 * 64 + 1 + 1)
		if(this.cr&2) this.hash.update(12 * 64 + 1 + 2)
		if(this.cr&4) this.hash.update(12 * 64 + 1 + 3)
		if(this.cr&8) this.hash.update(12 * 64 + 1 + 4)
		
		return this
	}

	/**
	 * 
	 * @param {String} san 
	 * @param {Array} validmoves
	 * @returns {Number} 
	 */
	sanToMove(san, validmoves = this.validMoves) {
		san = san.replace(/(\W|x)*/g,'')

		if(san == "OO" || san == "00") {
			const from = this.tomove==chessalpha.BLACK ? 40:152
			return this._generateMove(from, from + 2, chessalpha.CastleKing)
		}
		if(san == "OOO" || san == "000") {
			const from = this.tomove==chessalpha.BLACK ? 40:152
			return this._generateMove(from, from - 2, chessalpha.CastleQueen)
		}

		san = san[0] + san.slice(1).toLowerCase()
		
		const matches = san.match(/([PNBRQK])?([a-h])?([1-8])?([a-h][1-8])([qrbn])?/)
		const line = matches[2] ? matches[2].charCodeAt(0) - 93:0
		const	row = matches[3] ? 10-matches[3]<<4:0
    const to = chessalpha.formatSquare(matches[4])
		const promotion = ({n:chessalpha.PromoteKnight,b:chessalpha.PromoteBishop,r:chessalpha.PromoteRook,q:chessalpha.PromoteQueen})[matches[5]]||0
		
		let piece = chessalpha.formatPiecestring(chessalpha.formatCharToPiece((matches[1]||'p').toLowerCase()))
		piece = piece.charAt(0).toUpperCase() + piece.slice(1)

		const pieceList = line && row ? this.pieceListFrom(line|row):this.pieceList
		const	moves = this['_captureMoves' + piece](this['_quiteMoves' + piece]([], pieceList), pieceList).filter(move => { return !new Position(this).move(move)._inCheck(this.tomove) })

		let i = moves.length
		if(line && row) { while(i--) if(((moves[i]>>8)&0xFF) == to && (moves[i]&0xF) == line && (moves[i]&0xF0) == row && ((moves[i]>>16)&chessalpha.Promotion)==promotion) return moves[i] }
		else if(line) { while(i--) if(((moves[i]>>8)&0xFF) == to && (moves[i]&0xF) == line && ((moves[i]>>16)&chessalpha.Promotion)==promotion) return moves[i] }
		else if(row) { while(i--) if(((moves[i]>>8)&0xFF) == to && (moves[i]&0xF0) == row && ((moves[i]>>16)&chessalpha.Promotion)==promotion) return moves[i] }
		else { while(i--) if(((moves[i]>>8)&0xFF) == to && ((moves[i]>>16)&chessalpha.Promotion)==promotion) return moves[i] }

    return null
	}

	/**
	 * 
	 * @param {Number} move 
	 * @param {Array} validmoves 
	 * @returns {String}
	 */
	moveToSan(move, validmoves = this.validMoves) {
		if(move==0) return "--"
		if(move==1) return "*"

		const [ from, to, flags ] = this._splitMove(move)

		if(from<36) return chessalpha.formatPieceToChar(from&7).toUpperCase() + "@" + chessalpha.formatSan(to)
		if(to<36) return "-" + chessalpha.formatSan(from)

    if(!~validmoves.indexOf(move)) return chessalpha.formatSan(from) + chessalpha.formatSan(to) + (flags & chessalpha.Promotion ? ({"16":"=R","32":"=N","64":"=Q","128":"=B"})[flags&chessalpha.Promotion]:'')

		if (flags & chessalpha.CastleKing) return "O-O"
		if (flags & chessalpha.CastleQueen) return "O-O-O"

		const board = this.board
		const	piece = board[from]

		let san = chessalpha.formatPieceToChar(piece&7).toUpperCase()
    let dupe = false
    let rowDiff = true
    let colDiff = true
	  let i = validmoves.length

		while(i--) {
			const moveFrom = validmoves[i] & 0xFF
			const	moveTo = (validmoves[i] >> 8) & 0xFF
			if (moveFrom != from && moveTo == to && board[moveFrom] == piece) {
				dupe = true
				if ((moveFrom & 0xF0) == (from & 0xF0)) {
					rowDiff = false
				}
				if ((moveFrom & 0x0F) == (from & 0x0F)) {
					colDiff = false
				}
			}
		}

		const conflict = chessalpha.formatSan(from)
		if (dupe) san += colDiff ? conflict[0]:rowDiff ? conflict[1]:conflict
		else if ((piece&7) == chessalpha.PAWN && (board[to] != 0 || (flags & chessalpha.EPC))) san += conflict[0]

		if (board[to] != 0 || (flags & chessalpha.EPC)) san += "x"

		san += chessalpha.formatSan(to)

		if (flags & chessalpha.Promotion) san += ({"16":"=R","32":"=N","64":"=Q","128":"=B"})[flags&chessalpha.Promotion]

		const position = new Position(this).move(move)
		if(position.inCheck) san += position.validMoves.length == 0 ? "#" : "+"

		return san
	}

	/**
	 * @returns {String}
	 */
	get ascii() {
		let square = 0, piece, s = '\n   +------------------------+\n'
		for(let row=0; row<8; row++) {
			s += ' '+(8-row)+' |'
			for(let col=0; col<8; col++) {
				piece = this.board[16 * (square>>3) + (square&7) + 36]
				if(!piece) s += ' . '
				else s += ' '+['p','n','b','r','q','k'][(piece&7)-1][piece&chessalpha.BLACK ? 'toLowerCase':'toUpperCase']()+' '
				square++
			}
			s += '|\n'
		}
		s += '   +------------------------+\n'
		s += '     a  b  c  d  e  f  g  h\n'
		return s
	}

	/**
	 * @returns {String}
	 */
	get key() {
		let i = 64, key = ""
		for(let i = 0; i<64; i++)
			key += chessalpha.formatPieceToFen(this.board[chessalpha.format64To192(i)])
		key += String.fromCharCode(
			this.cr+97,
			this.eps&0xF-4+97,
			108.5-(this.tomove-12)/(4/10.5)
		)
		return key
	}

	/**
	 * @returns {Hash}
	 */
	generateHash() {
		const hash = new Hash()
		let piece
		let i = 64
		while(i--) {
			piece = this.board[chessalpha.format64To192(i)]
			if(piece) hash.update(chessalpha.formatPieceToHash(piece)*64+i)
		}

		if(this.tomove===chessalpha.BLACK) hash.update(12 * 64 + 1)

		if(this.cr&1) hash.update(12 * 64 + 1 + 1)
		if(this.cr&2) hash.update(12 * 64 + 1 + 2)
		if(this.cr&4) hash.update(12 * 64 + 1 + 3)
		if(this.cr&8) hash.update(12 * 64 + 1 + 4)

		if(~this.eps) hash.update(12 * 64 + 1 + 4 + this.eps&0xF - 4)

		return hash
	}

}

/**
 * 
 * @param {Number} seed 
 * @returns {Number}
 */
function Random(seed) {
  const c = 4294967295

	this._seed = seed % c

	// Returns a pseudo-random value between 1 and 2^32 - 2.
	this.next = multiple => {
		return this._seed = this._seed * multiple % c
	}
}

class Hash {

	/**
	 * 
	 * @param {Number} low 
	 * @param {Number} high 
	 */
	constructor(low, high) {
		let count = 12 * 64 + 1 + 4 + 8 // pieces * squares + tomove + castlerights + ep lines
		const lowRandom = new Random(0x1BADF00D)
		const highRandom = new Random(0xC11E55)
		this.lowKeys = new Uint32Array(count)
		this.highKeys = new Uint32Array(count)
		
		while(count--) {
			this.lowKeys[count] = lowRandom.next(16807 + count)
			this.highKeys[count] = highRandom.next(16807 - count)
		}

		this.low = 0
		this.high = 0
	}

	/**
	 * 
	 * @param {Number} value 
	 */
	update(value) {
		this.low ^= this.lowKeys[value]
		this.high ^= this.highKeys[value]
	}

	/**
	 * 
	 * @param {Hash} hash 
	 * @returns {Boolean}
	 */
	equals(hash) {
		return this.low == hash.low && this.high == hash.high
	}

	/**
	 * @returns {String}
	 */
	toString() {
		return `${this.low},${this.high}`
	}
}

export default Position