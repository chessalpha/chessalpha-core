const chessalpha = {
  WHITE: 8,
  BLACK: 16,

  EMPTY: 0x00,
  PAWN: 0x01,
  KNIGHT: 0x02,
  BISHOP: 0x03,
  ROOK: 0x04,
  QUEEN: 0x05,
  KING: 0x06,

  EPC: 0x2,
  CastleKing: 0x4,
  CastleQueen: 0x8,
  PromoteRook: 0x10,
  PromoteKnight: 0x20,
  PromoteQueen: 0x40,
  PromoteBishop: 0x80,
  Promotion: 0xf0,

  status: {
    CREATED: 10,
    ACCEPTED: 15,
    STARTED: 20,
    ABORTED: 25,
    STOPPED: 27,
    MATE: 30,
    RESIGN: 31,
    TIMEOUT: 32, // when player leaves the game
    OUTOFTIME: 33, // clock flag
    DRAW: 40,
    STALEMATE: 41,
    MOVE50: 42,
    THREEFOLD: 43,
    INSUFFICIENTMATERIAL: 44,
    UNKNOWN: 50,
  },

  result: {
    ONGOING: 0,
    DRAW: 0x04,
    WHITEWIN: 0x08,
    BLACKWIN: 0x10,
    UNKNOWN: 0x18,
  },

  nag: [
    {
      meaning: "null annotation",
    },
    {
      symbol: "!",
      meaning: "good move",
    },
    {
      symbol: "?",
      meaning: "poor move",
    },
    {
      symbol: "!!",
      meaning: "very good move",
    },
    {
      symbol: "??",
      meaning: "blunder",
    },
    {
      symbol: "!?",
      meaning: "interesting move",
    },
    {
      symbol: "?!",
      meaning: "dubious move",
    },
    {
      symbol: "□",
      meaning: "forced move",
    },
    {
      meaning: "singular move",
    },
    {
      meaning: "worst move",
    },
    {
      symbol: "=",
      meaning: "even position",
    },
    {
      meaning: "quiet position",
    },
    {
      meaning: "active position",
    },
    {
      symbol: "∞",
      meaning: "unclear position",
    },
    {
      symbol: "⩲",
      meaning: "White has a slight advantage",
    },
    {
      symbol: "⩱",
      meaning: "Black has a slight advantage",
    },
    {
      symbol: "±",
      meaning: "White has a moderate advantage",
    },
    {
      symbol: "∓",
      meaning: "Black has a moderate advantage	",
    },
    {
      symbol: "+−",
      meaning: "White has a decisive advantage",
    },
    {
      symbol: "-+",
      meaning: "Black has a decisive advantage",
    },
    {
      meaning: "White has a crushing advantage",
    },
    {
      meaning: "Black has a crushing advantage",
    },
    {
      symbol: "⨀",
      meaning: "White is in zugzwang",
    },
    {
      symbol: "⨀",
      meaning: "Black is in zugzwang",
    },
    {
      meaning: "White has a slight space advantage",
    },
    {
      meaning: "Black has a slight space advantage",
    },
    {
      meaning: "White has a moderate space advantage",
    },
    {
      meaning: "Black has a moderate space advantage",
    },
    {
      meaning: "White has a decisive space advantage",
    },
    {
      meaning: "Black has a decisive space advantage",
    },
    {
      meaning: "White has a slight time advantage",
    },
    {
      meaning: "Black has a slight time advantage",
    },
    {
      symbol: "⟳",
      meaning: "White has a moderate time advantage",
    },
    {
      symbol: "⟳",
      meaning: "Black has a moderate time advantage",
    },
    {
      meaning: "White has a decisive time advantage",
    },
    {
      meaning: "Black has a decisive time advantage",
    },
    {
      symbol: "→",
      meaning: "White has the initiative",
    },
    {
      symbol: "→",
      meaning: "Black has the initiative",
    },
    {
      meaning: "White has a lasting initiative",
    },
    {
      meaning: "Black has a lasting initiative",
    },
    {
      symbol: "↑",
      meaning: "White has the attack",
    },
    {
      symbol: "↑",
      meaning: "Black has the attack",
    },
    {
      meaning: "White has insufficient compensation for material deficit",
    },
    {
      meaning: "Black has insufficient compensation for material deficit",
    },
    {
      meaning: "White has sufficient compensation for material deficit",
    },
    {
      meaning: "Black has sufficient compensation for material deficit",
    },
    {
      meaning: "White has more than adequate compensation for material deficit",
    },
    {
      meaning: "Black has more than adequate compensation for material deficit",
    },
    {
      meaning: "White has a slight center control advantage",
    },
    {
      meaning: "Black has a slight center control advantage",
    },
    {
      meaning: "White has a moderate center control advantage",
    },
    {
      meaning: "Black has a moderate center control advantage",
    },
    {
      meaning: "White has a decisive center control advantage",
    },
    {
      meaning: "Black has a decisive center control advantage",
    },
    {
      meaning: "White has a slight kingside control advantage",
    },
    {
      meaning: "Black has a slight kingside control advantage",
    },
    {
      meaning: "White has a moderate kingside control advantage",
    },
    {
      meaning: "Black has a moderate kingside control advantage",
    },
    {
      meaning: "White has a decisive kingside control advantage",
    },
    {
      meaning: "Black has a decisive kingside control advantage",
    },
    {
      meaning: "White has a slight queenside control advantage",
    },
    {
      meaning: "Black has a slight queenside control advantage",
    },
    {
      meaning: "White has a moderate queenside control advantage",
    },
    {
      meaning: "Black has a moderate queenside control advantage",
    },
    {
      meaning: "White has a decisive queenside control advantage",
    },
    {
      meaning: "Black has a decisive queenside control advantage",
    },
    {
      meaning: "White has a vulnerable first rank",
    },
    {
      meaning: "Black has a vulnerable first rank",
    },
    {
      meaning: "White has a well protected first rank",
    },
    {
      meaning: "Black has a well protected first rank",
    },
    {
      meaning: "White has a poorly protected king",
    },
    {
      meaning: "Black has a poorly protected king",
    },
    {
      meaning: "White has a well protected king",
    },
    {
      meaning: "Black has a well protected king",
    },
    {
      meaning: "White has a poorly placed king",
    },
    {
      meaning: "Black has a poorly placed king",
    },
    {
      meaning: "White has a well placed king",
    },
    {
      meaning: "Black has a well placed king",
    },
    {
      meaning: "White has a very weak pawn structure",
    },
    {
      meaning: "Black has a very weak pawn structure",
    },
    {
      meaning: "White has a moderately weak pawn structure",
    },
    {
      meaning: "Black has a moderately weak pawn structure",
    },
    {
      meaning: "White has a moderately strong pawn structure",
    },
    {
      meaning: "Black has a moderately strong pawn structure",
    },
    {
      meaning: "White has a very strong pawn structure",
    },
    {
      meaning: "Black has a very strong pawn structure",
    },
    {
      meaning: "White has poor knight placement",
    },
    {
      meaning: "Black has poor knight placement",
    },
    {
      meaning: "White has good knight placement",
    },
    {
      meaning: "Black has good knight placement",
    },
    {
      meaning: "White has poor bishop placement",
    },
    {
      meaning: "Black has poor bishop placement",
    },
    {
      meaning: "White has good bishop placement",
    },
    {
      meaning: "Black has good bishop placement",
    },
    {
      meaning: "White has poor rook placement",
    },
    {
      meaning: "Black has poor rook placement",
    },
    {
      meaning: "White has good rook placement",
    },
    {
      meaning: "Black has good rook placement",
    },
    {
      meaning: "White has poor gueen placement",
    },
    {
      meaning: "Black has poor gueen placement",
    },
    {
      meaning: "White has good gueen placement",
    },
    {
      meaning: "Black has good gueen placement",
    },
    {
      meaning: "White has poor piece coordination",
    },
    {
      meaning: "Black has poor piece coordination",
    },
    {
      meaning: "White has good piece coordination",
    },
    {
      meaning: "Black has good piece coordination",
    },
    {
      meaning: "White has played the opening very poorly",
    },
    {
      meaning: "Black has played the opening very poorly",
    },
    {
      meaning: "White has played the opening poorly",
    },
    {
      meaning: "Black has played the opening poorly",
    },
    {
      meaning: "White has played the opening well",
    },
    {
      meaning: "Black has played the opening well",
    },
    {
      meaning: "White has played the opening very well",
    },
    {
      meaning: "Black has played the opening very well",
    },
    {
      meaning: "White has played the middlegame very poorly",
    },
    {
      meaning: "Black has played the middlegame very poorly",
    },
    {
      meaning: "White has played the middlegame poorly",
    },
    {
      meaning: "Black has played the middlegame poorly",
    },
    {
      meaning: "White has played the middlegame well",
    },
    {
      meaning: "Black has played the middlegame well",
    },
    {
      meaning: "White has played the middlegame very well",
    },
    {
      meaning: "Black has played the middlegame very well",
    },
    {
      meaning: "White has played the ending very poorly",
    },
    {
      meaning: "Black has played the ending very poorly",
    },
    {
      meaning: "White has played the ending poorly",
    },
    {
      meaning: "Black has played the ending poorly",
    },
    {
      meaning: "White has played the ending well",
    },
    {
      meaning: "Black has played the ending well",
    },
    {
      meaning: "White has played the ending very well",
    },
    {
      meaning: "Black has played the ending very well",
    },
    {
      meaning: "White has slight counterplay",
    },
    {
      meaning: "Black has slight counterplay",
    },
    {
      symbol: "⇆",
      meaning: "White has moderate counterplay",
    },
    {
      symbol: "⇆",
      meaning: "Black has moderate counterplay",
    },
    {
      meaning: "White has decisive counterplay",
    },
    {
      meaning: "Black has decisive counterplay",
    },
    {
      meaning: "White has moderate time control pressure",
    },
    {
      meaning: "Black has moderate time control pressure",
    },
    {
      meaning: "White has severe time control pressure",
    },
    {
      meaning: "Black has severe time control pressure",
    },
  ],

  /**
   * Converts a square192 to a san string
   *
   * @param {string} san
   * @returns {Number} square192
   */
  formatSquare: function(san) {
    return (san.charCodeAt(0) - 93) | ((10 - san[1]) << 4);
  },

  /**
   * Converts a san to a square192
   *
   * @param {Number} square192
   * @returns {String} san
   */
  formatSan: function(square) {
    return "abcdefgh"[(square & 0xf) - 4] + (9 - (square >> 4) + 1);
  },

  /**
   * Converts a square64 to a square192
   *
   * @param {Number} square64
   * @returns {Number} square192
   */
  format64To192: function(square64) {
    return 16 * (square64 >> 3) + (square64 & 7) + 36;
  },

  /**
   * Converts a square192 to a square64
   *
   * @param {Number} square192
   * @returns {Number} square64
   */
  format192To64: function(square) {
    return (square & (0xf - 4)) | (((square & 0xf0) >> 1) - 16);
  },

  /**
   * Converts a piece string to a piece number
   *
   * @param {String}
   * @param {String}
   * @returns {Number}
   */
  formatPiece: function(type, color) {
    return (
      chessalpha[type.toUpperCase()] |
      (color ? chessalpha[color.toUpperCase()] : 0)
    );
  },

  /**
   * Converts a piece number to a char
   *
   * @param {Number}
   * @param {Boolean}
   * @returns {String}
   */
  formatPieceToChar: function(piece, usePawn) {
    return ["", usePawn ? "p" : "", "n", "b", "r", "q", "k"][piece & 7];
  },

  /**
   * Converts a piece number to a fen char
   *
   * @param {Number}
   * @returns {String}
   */
  formatPieceToFen: function(piece) {
    return piece
      ? this.formatPieceToChar(piece & 7, true)[
          piece & chessalpha.BLACK ? "toLowerCase" : "toUpperCase"
        ]()
      : "-";
  },

  /**
   * Converts a piece number to a piece hash number (0-5)
   *
   * @param {Number}
   * @returns {Number}
   */

  formatPieceToHash: function(piece) {
    return (piece & 7) - 1 + (piece & chessalpha.BLACK) * 0.375;
  },

  /**
   * Converts a piece char to a piece number
   *
   * @param {String}
   * @returns {Number}
   */
  formatCharToPiece: function(char) {
    return {
      p: chessalpha.PAWN,
      n: chessalpha.KNIGHT,
      b: chessalpha.BISHOP,
      r: chessalpha.ROOK,
      q: chessalpha.QUEEN,
      k: chessalpha.KING,
    }[char];
  },

  /**
   * Converts a piece number to a piece string
   *
   * @param {Number}
   * @returns {String}
   */
  formatPiecestring: function(piece) {
    return ["", "pawn", "knight", "bishop", "rook", "queen", "king"][piece & 7];
  },

  /**
   * Converts a color number to a color string
   *
   * @param {Number}
   * @returns {String}
   */
  formatColorstring: function(color) {
    return ["white", "black"][(color & 24) >> 4];
  },

  /**
   * Converts a color string to a color number
   *
   * @param {String}
   * @returns {Number}
   */
  formatColor: function(colorstring) {
    return { w: chessalpha.WHITE, b: chessalpha.BLACK }[
      colorstring[0].toLowerCase()
    ];
  },

  /**
   * Converts a result string to a result number
   *
   * @param {String}
   * @returns {Number}
   */
  formatResult: function(resultstring) {
    return (
      {
        "1/2-1/2": chessalpha.result.DRAW,
        "½-½": chessalpha.result.DRAW,
        "1-0": chessalpha.result.WHITEWIN,
        "0-1": chessalpha.result.BLACKWIN,
      }[resultstring] || 0
    );
  },

  /**
   * Converts a result number to a result string
   *
   * @param {Number}
   * @returns {String}
   */
  formatResultstring: function(result) {
    return { "4": "½-½", "8": "1-0", "16": "0-1" }[result];
  },

  /**
   * Converts a square number to a xy number
   *
   * @param {Number}
   * @returns {Object} xyObject
   */
  formatXY: function(square) {
    return { x: (square & 0xf) - 4, y: (square >> 4) - 2 };
  },

  /**
   * Converts a xy number to a san square
   *
   * @param {Object} xyObject
   * @returns {String}
   */
  formatXYToSan: function(xy) {
    return ["a", "b", "c", "d", "e", "f", "g", "h"][xy.x] + (8 - xy.y);
  },

  /**
   * Converts a xy number to a square number
   *
   * @param {Object} xyObject
   * @returns {Number}
   */
  formatXYToSquare: function(xy) {
    return (xy.x + 4) | ((xy.y + 2) << 4);
  },

  /**
   * Converts a san square to a xy number
   *
   * @param {String}
   * @returns {Object} xyObject
   */
  formatSanToXY: function(san) {
    return { x: san.charCodeAt(0) - 97, y: 8 - san[1] };
  },
};

export default chessalpha;
