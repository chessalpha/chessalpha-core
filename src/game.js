import chessalpha from './core.js'
import Position from './position.js'
import { Movelist, Node } from'./movelist.js'

const RE = {
	TAG: new RegExp(/\[([^\]]*)\s"([^\]]*)"\]/),
	NAG: new RegExp(/^\$([\d]){1,3}/),
	STARTMOVE: new RegExp(/(^[\d]+)[\.](?!\.)/),
	BLACKMOVE: new RegExp(/(^[\d]+)[\.]{3}/),
	COMMENT: new RegExp(/^\{([^\}]*)\}/),
	COMMENTTAG: new RegExp(/\[\%(.*)\s(.*)\]/),
	ALTERNATE: new RegExp(/^\(([^\)]*)\)/),
	ALTERNATESTART: new RegExp(/(\()/),
	ALTERNATEEND: new RegExp(/(\))/),
	MOVE: new RegExp(/^(?:[KQRNBP]?[a-h]?[1-8]?x?@?-?[a-h][1-8]\=?[KQRNBP]?|O(-?O){1,2}|\-\-)[\+#]?(\s*[\!\?]+)?/),
	END: new RegExp(/(1\/2-1\/2|1-0|0-1|\*)/)
}

/**
 * Finds start and end of subline in pgn
 * 
 * @param {String}
 * @returns {String}
 */
function findMatchingParen(str) {
	let i = 0, chr, left = 0, right = 0

	while (chr = str.charAt(i++)) {

		if (RE.ALTERNATESTART.test(chr)) {
			left++
		}
		else if (RE.ALTERNATEEND.test(chr)) {
			right++
		}

		if (left == right && left != 0) {
			return str.substring(1, i-1)
		}
	}

	return ''
}

class Game {

	/**
	 * 
	 * @param {String} pgn 
	 * @param {Object} options 
	 * @returns {Boolean}
	 */
	static parsePgn(pgn, options = {}) {
		let match, prev, created = false

		this.__setPgnCommentAfter = false
		this.__setPgnCommentBefore = ''

		while (true) {
			pgn = pgn.trim()

			// tag
			if (match = pgn.match(RE.TAG)) {
				if(!created) {
					if(options.onCreate) options.onCreate()
					created = true
				}

				const name = match[1].toLowerCase()
				const	value  = match[2]

				if(name && value && options.onTag) options.onTag({name, value})
				else if(options.debug) console.error('Invalid PGN: Malformed tag')

				pgn = pgn.replace(match[0], '')
			}

			// start of a new move set
			else if (match = pgn.match(RE.STARTMOVE)) {
				if(options.onStartMove) options.onStartMove(match[0])
				pgn = pgn.replace(match[0], '').trim()
			}

			// move
			else if (match = pgn.match(RE.MOVE)) {
				if(options.onMove) options.onMove(match[0])
				pgn = pgn.replace(match[0], '').trim()
			}

			// black
			else if (match = pgn.match(RE.BLACKMOVE)) {
				if(options.onBlackMove) options.onBlackMove(match[0])
				pgn = pgn.replace(match[0], '').trim()
			}

			// nag
			else if (match = pgn.match(RE.NAG)) {
				if(options.onNag) options.onNag(match[0])
				pgn = pgn.replace(match[0], '').trim()
			}
			
			// comment
			else if (match = pgn.match(RE.COMMENT)) {
				let commentMatch
				let comment = match[1]
				const originalComment = comment
				const tags = {}
				while(true) {
					if(commentMatch = comment.match(RE.COMMENTTAG)) {
						tags[commentMatch[1].toLowerCase()] = commentMatch[2]
						comment = comment.replace(commentMatch[0], '').trim()
					}
					if (comment === prev) break
					if (comment.length < 1) break

					commentMatch = null
					prev = comment
				}
				
				if(options.onComment) options.onComment(comment, tags, originalComment)
				pgn = pgn.replace(match[0], '').trim()
			}

			// alternate line
			else if (match = pgn.match(RE.ALTERNATE)) {
				const altStr = findMatchingParen(pgn)
				if(options.onAlternate) options.onAlternate(altStr)
				pgn = pgn.replace('('+altStr+')', '').trim()
			}

			// game decision
			else if (match = pgn.match(RE.END)) {
				if(options.onDecision) options.onDecision(match[0])
				pgn = pgn.replace(match[0], '').trim()
			}

			// nothing left to parse?
			if (pgn.length < 1) {
				if(options.onComplete) options.onComplete()
				if(options.debug) console.info('Parsing Complete!')
				break
			}

			// break out of loop if no rule matched
			if (pgn === prev) {
				if(options.debug) console.error('Invalid PGN: Unknown error', 'string: '+pgn)
				return false
			}

			match = null
			prev = pgn
		}

		return true
	}

	/**
	 * 
	 * @param {Game} game
	 * @returns {Object} 
	 */
	static pgnOptions(game) {
		const options = {
			onTag: ({name, value}) => {
				if(~["event","site"].indexOf(name)) {
					game[name] = value
					return
				}
				if(name==="round") {
					const split = value.split(".")
					game.round = +split[0]
					if(split[1]) game.board = +split[1]
					return
				}
				if(name==="board") {
					game.board = +value
					return
				}
				if(name==="date") {
					game.date = new Date(value)
					return
        }
				if(name=="fen") {
					game.initPosition([new Node({id: 0, position: value, start: true})])
					return
				}

				if(~["white","black"].indexOf(name)) {
					let i = value.indexOf(",") + 1
					if(i) value = value.substr(i).trim() + " " + value.substr(0, i - 1)
				}

				if(name.startsWith("white")) {
					game.white = game.white||{}
					game.white[name.substr(5)||"name"] = value
				}
				else if(name.startsWith("black")) {
					game.black = game.black||{}
					game.black[name.substr(5)||"name"] = value
				}

			},

			onMove: san => {
				if(!game.move(san)) throw new Error(game.position.ascii + "color: " + game.position.tomove + "|move not possible " + san + ", " + game.position.validMoves.map(move => game.position.moveToSan(move)))
				if(game.__setPgnCommentBefore) {
					game.current.textbefore = game.__setPgnCommentBefore
					game.__setPgnCommentBefore = ''
				}
				game.__setPgnCommentAfter = true
			},

			onNag: nag => {
				game.current.nags = game.current.nags||[]
				game.current.nags.push(nag)
			},

			onAlternate: str => {
				game.__setPgnCommentAfter = false
				let c = game.current
				let	p = new Position(game.position)

				game.current = game.current.prev
				game.position = game.current.position

				Game.parsePgn(str, options)

				game.current = c
				game.position = p
			},

			onComment: (comment, tags) => {
				comment = comment.replace(/(?:\r\n|\r|\n)/g, ' ')
				if(game.__setPgnCommentAfter) game.current.textafter = comment
				else game.__setPgnCommentBefore = comment
				game.__setPgnCommentAfter = false
				
				if(tags) {
					if(tags.clk) {
						if(game.current.prev) {
							const color1 = chessalpha.formatColorstring(game.position.tomove) + "time"
							game.current[color1] = game.current.prev[color1]
						}
						game.current[chessalpha.formatColorstring(game.position.tomove^24) + "time"] = (([hours,minutes,seconds]) => { return ((+hours * 60 + +minutes) * 60 + +seconds) * 1000 })(tags.clk.split(':'))
					}
				}
			},

			onDecision: result => {
				game.result = chessalpha.formatResult(result)
				game.status = game.result ? chessalpha.status.UNKNOWN:game.current.start ? chessalpha.status.CREATED:chessalpha.status.STARTED

				// node missing time
				game.movelist.list.forEach(node => {
					// TODO: if game is ongoing, calc time based on timestamp
					if(node.prev) {
						if(!node.whitetime) node.whitetime = node.prev.whitetime
						if(!node.blacktime) node.blacktime = node.prev.blacktime
					}
				})
			}
		}
		return options
	}

	/**
	 * 
	 * @param {Game|String} game 
	 */
	constructor(game) {
		if(typeof game=="string") {
			this.status = 0
			this.result = 0
			this.pgn = game
		}
		else 
			this.init(game||{})
	}

	/**
	 * 
	 * @param {Game} game 
	 */
	init(game) {
		for(let key in game) this[key] = game[key]
		
		this.initPosition(game.movelist, game.current||0)

		this.white = this.white||{}
		this.black = this.black||{}
		
		this.result = game.result||0
		this.status = game.status ? game.status:this._status()
	}

	/**
	 * 
	 * @param {Array} movelist 
	 * @param {Node} current 
	 */
	initPosition(movelist, current = 0) {
		this.movelist = new Movelist(movelist||[new Node({id: 0, position:'start', start: true})])
		this.current = typeof current === "number" ? this.movelist.list[current]:current
		this.position = this.current.position

		if(this.timecontrol) Object.assign(this.current, this.movetime())
	}

	/**
	 * @returns {String}
	 */
	get pgn() {

		const result = ({"4":"1/2-1/2","8":"1-0","16":"0-1"})[this.result]||'*'
		let tags = ""
		let moves = ""
		let current = this.movelist.first()
		let	position = current.position

		if(this.event) tags += `[Event "${this.event}"]\n`
		if(this.site) tags += `[Site "${this.site}"]\n`
		if(this.date) tags += `[Date "${this.date.getFullYear()}.${("0"+(this.date.getMonth()+1)).slice(-2)}.${("0"+this.date.getDate()).slice(-2)}"]\n`
		if(this.round) tags += `[Round "${this.round}"]\n`
		if(this.board) tags += `[Board "${this.board}"]\n`
		tags += '[White "' + (this.white.name||'') + '"]\n'
		tags += '[Black "' + (this.black.name||'') + '"]\n'
		tags += '[Result "' + result + '"]\n'

		if(!position.hash.equals(new Position('start').hash)) tags += '[FEN "' + position.fen + '"]\n'

		/**
		 * 
		 * @param {Node} node 
		 * @param {Number} halfmove 
		 * @param {Boolean} showNumber 
		 * @returns {String}
		 */
		function move(node, halfmove, showNumber) {
			const isWhite = halfmove % 2
			
			let ms = node[(isWhite ? "white":"black") + "time"]
			let time = ""
			if(ms) {
				ms = Math.floor(ms / 1000)
				const h = Math.floor(ms / 3600)
				const m = Math.floor((ms - (h * 3600)) / 60)
				const s = Math.floor(ms - (h * 3600) - (m * 60))
				time = '[%clk '+h+':'+('0'+m).slice(-2)+':'+('0'+s).slice(-2)+']'
			}
			let textafter = node.textafter ? time+' '+node.textafter.trim():time
			return (node.textbefore ? "{" + node.textbefore.trim() + "} ":"") + 
				(showNumber || isWhite ? (~~(halfmove/2) + isWhite) + (isWhite ? ". ":"... "):'') + node.san + 
				(node.nags ? ' ' + node.nags.reduce((str, nag) => str + nag, ''):'') + " " +
				(textafter ? "{" + textafter + "} ":"")
		}

		(function loop(nodes, halfmove, showNumber) {
			let i=0
			if(nodes[i]) {
				halfmove += 1
				
				moves += move(nodes[i], halfmove, showNumber)
				while(++i<nodes.length) {
					moves += '(' + move(nodes[i], halfmove, nodes.length > 1)
					loop(nodes[i].next, halfmove, false)
					moves = moves.substr(0, moves.length - 1) + ') '
				}
				loop(nodes[0].next, halfmove, nodes.length > 1)
			}
		})(current.next, position.halfmove, true)

		let lastBlankspace
		for(let i=1;i<moves.length;i++) {
			if(moves.charAt(i) == " ") lastBlankspace = i
			if(!(i%80)) moves = moves.substr(0,lastBlankspace) + '\n' + moves.substr(lastBlankspace + 1)
		}
		return tags + '\n' + moves + result
	}

	/**
	 * @param {String}
	 */
	set pgn(pgn) {
		/*
		 * VALIDTAGS
		 * ['event', 'site', 'date', 'round', 'white',
		 * 'black', 'result', 'eco', 'whitefideid', 'blackfideid', 'whitetitle', 'blacktitle', 'whiteelo', 'blackelo',
		 * 'plycount', 'eventdate', 'fen']
		 */

		if(!this.position || !this.current) this.initPosition()

		this.constructor.parsePgn(pgn, this.constructor.pgnOptions(this))
	}

	/**
	 * 
	 * @param {Number} move 
	 * @param {Array} validmoves 
	 * @param {Node} current 
	 * @param {Number} index 
	 * @returns {Game}
	 */
	move(move, validmoves = this.position.validMoves, current = this.current, index) {
		let san

		if(+move!==move) {
			san = move
			move = this.position.sanToMove(move)
		}

		if(~validmoves.indexOf(move)) {
			this.current = this.movelist.move(move, current, san||this.position.moveToSan(move), index)
			this.position.move(move)
			if(this.timecontrol) Object.assign(this.current, this.movetime())

			if(this.status < chessalpha.status.ABORTED) {
				this.status = this._status()||this.status // this._status() returns 0 if not decided and movenr<2
				this.result = this._result()||this.result
			}

			return this
		}

		return null
	}

	/**
	 * 
	 * @param {Node} node 
	 * @param {Number} halfmoveClockStart 
	 * @returns {Boolean}
	 */
	withinStartRange(node = this.current, halfmoveClockStart = this.halfmoveClockStart||3) {
		do { if(node.start) return true } 
		while(--halfmoveClockStart && (node = node.prev))
		return false
	}

	/**
	 * 
	 * @param {Array} timecontrol 
	 * @param {Number} timestamp 
	 * @param {Node} node 
	 * @param {Number} tomove 
	 * @param {Number} halfmoveClockStart 
	 * @returns {Object}
	 */
	movetime(timecontrol = this.timecontrol, timestamp = Date.now(), node = this.current, tomove = this.position.tomove, halfmoveClockStart = this.halfmoveClockStart||3) {
		const prev = node.prev
		const increment = timecontrol[0].increment||0
		let { whitetime, blacktime } = prev||{}

		if(this.withinStartRange(node, halfmoveClockStart)) {
			whitetime = prev&&prev.whitetime||node.whitetime||timecontrol[0].start
			blacktime = prev&&prev.blacktime||node.blacktime||timecontrol[0].start
		}
		else if(prev) {
      const time = prev.timestamp - timestamp + increment
      if(tomove==chessalpha.WHITE) blacktime += time
      else whitetime += time
    }
    return { timestamp, whitetime, blacktime }
	}

	/**
	 * 
	 * @param {Number} moveid 
	 * @returns {Game}
	 */
	goto(moveid) {
		this.current = this.movelist.list[moveid]
		this.position = this.current.position
		return this
  }
	
	/**
	 * 
	 * @param {Number} index 
	 * @returns {Game}
	 */
	next(index) {
		index=index||0
		if(this.current.next[index]) {
			this.current = this.current.next[index]
			this.position = this.current.position
		}
		return this
	}

	/**
	 * @returns {Game}
	 */
	prev() {
		if(this.current.prev) {
			this.current = this.current.prev
      this.position = this.current.position
		}
		return this
	}

	/**
	 * 
	 * @param {Node} current 
	 * @returns {Number}
	 */
	_status(current = this.current) {
		let status = current.status||this.result&&chessalpha.status.UNKNOWN||0
		if(status < chessalpha.status.ABORTED && this.timecontrol) {
			if(this.current[chessalpha.formatColorstring(this.position.tomove) + 'time']<1) {
				status = this.position._pieceList(this.position.tomove^24)[0].length>1 ? chessalpha.status.OUTOFTIME:chessalpha.status.INSUFFICIENTMATERIAL
			}
		}
		return status
	}

	/**
	 * 
	 * @param {Number} status 
	 * @param {Position} position 
	 * @returns {Number}
	 */
	_result(status = this.status, position = this.position) {
		if(status < chessalpha.status.MATE) return 0
		if(status >= chessalpha.status.UNKNOWN) return chessalpha.result.UNKNOWN
		if(status < chessalpha.status.DRAW) return position.tomove^24
		return chessalpha.result.DRAW
	}

	/**
	 * @returns {Object}
	 */
	toJSON() {
		const obj = {}
		for(let key in this) {
			if(this[key]!=null) {
				if(this[key].toJSON) obj[key] = this[key].toJSON()
				else obj[key] = this[key]
			}
		}
		obj.current = obj.current.id
		return obj
	}

}

export default Game