import { strictEqual, ok } from "assert"
import chessalpha from '../src/core.js';
import Game from '../src/game.js';
import Position from '../src/position.js';

const fenSmithMorra = "rnbqkbnr/pp1ppppp/8/2p5/3PP3/8/PPP2PPP/RNBQKBNR b KQkq d3 0 2";
const pgn = `[Event "PÂskturneringen 2017"]
[Site "Norrköping"]
[Date "2017.04.17"]
[Round "8.2"]
[Board "2"]
[White "Brynell, GM Stellan"]
[Black "Arman, FM Deniz"]
[Result "1/2-1/2"]
[ECO "D09"]
[PlyCount "4"]
[EventDate "2017.??.??"]
[SetUp "1"]
[FEN "8/1k1K4/1p6/3P4/5Bn1/6P1/4r3/7R w - - 0 49"]

49.Rc1 $2 { [%clk 1:30:56] } ( { Efter } 49.Kd6 { har vit mycket goda vinstchanser. } ) ( 49.Kd8 )
49...Nf6+ 50.Kd6 ( 50.Kd8 Re8# { matt. } ( 50...Nxd5 ) ) 50...Ne8+ {  SÂ gick
det till n‰r Stellan missade segern i PÂskturneringen. } 1/2-1/2`;

describe("game", () => {

  it("constructor", () => {
    strictEqual(new Game(new Game({ status: chessalpha.status.STARTED })).status, chessalpha.status.STARTED)
    strictEqual(new Game({ status: chessalpha.status.UNKNOWN })._result(), chessalpha.result.UNKNOWN)
    strictEqual(new Game({ result: chessalpha.result.WHITEWIN })._status(), chessalpha.status.UNKNOWN)
  })

  describe("move", () => {
    it("Smith-Morra Gambit", () => {
      const game = new Game().
        move(chessalpha.formatSquare("e2")|(chessalpha.formatSquare("e4")<<8)).
        move("c5").
        move("d4")
      ok(game.position.fen==fenSmithMorra && game.movelist.list.length==4);
    })
    
  })

  describe("pgn", () => {

    const game = new Game(pgn)
        
    it("whitename", () => {
      ok(game.white.name == "GM Stellan Brynell");
    })
    it("blackname", () => {
      ok(game.black.name == "FM Deniz Arman");
    })
    it("result", () => {
      ok(game.result == chessalpha.result.DRAW);
    })
    it("status", () => {
      ok(game.status == chessalpha.status.UNKNOWN);
    })

    it("start fen", () => {
      ok(game.current.first.position.fen == "8/1k1K4/1p6/3P4/5Bn1/6P1/4r3/7R w - - 0 49");
    })
    it("end fen", () => {
      ok(game.current.last.position.fen == "4n3/1k6/1p1K4/3P4/5B2/6P1/4r3/2R5 w - - 4 51");
    })

    it("parse", () => {
      let create, startmove, blackmove, complete
      const result = Game.parsePgn(pgn, {
        debug: true,
        onCreate: () => { create = true },
        onStartMove: value => { startmove = value },
        onBlackMove: value => { blackmove = value },
        onComplete: () => { complete = true }
      })
      ok(result)
      ok(create)
      strictEqual(startmove, "50.") // last white move
      strictEqual(blackmove, "50...") // last black move
      ok(complete)

      const result2 = Game.parsePgn(`[TestTag "error"] 1. e4 d5 2. Bb5+ e5 ..`, Object.assign(Game.pgnOptions, {
        debug: true
      }))
      ok(!result2)

      const result3 = new Game("*")
      strictEqual(result3.status, chessalpha.status.CREATED)
      result3.pgn = "1. e4 *"
      strictEqual(result3.status, chessalpha.status.STARTED)
      
      strictEqual(Game.parsePgn("1. e4 { test *"), false)
    })

    it("generate", () => {
      const pgn2 = `[Event "Test"]
[Site "Test"]
[Date "2017.04.17"]
[Round "8"]
[Board "2"]
[White "GM Stellan Brynell"]
[Black "FM Deniz Arman"]
[Result "1/2-1/2"]
[FEN "8/1k1K4/1p6/3P4/5Bn1/6P1/4r3/7R w - - 0 49"]

49. Rc1 $2 {[%clk 1:30:56]} ({Efter} 49. Kd6 { har vit mycket goda
vinstchanser.}) (49. Kd8) 49... Nf6+ 50. Kd6 {[%clk 1:30:56]} (50. Kd8 {[%clk 1:30:56]} Re8#
{ matt.} (50... Nxd5)) 50... Ne8+ { SÂ gick det till n‰r Stellan missade segern
i PÂskturneringen.} 1/2-1/2`

      strictEqual(new Game(pgn2).pgn, pgn2)

      strictEqual(new Game({ status: chessalpha.status.STARTED }).pgn, `[White ""]
[Black ""]
[Result "*"]

*`)
    })

    // it("get pgn", function() {
    //   assert(game.pgn == pgn);
    // })
    
  })

  describe("threefold", () => {

    const game = new Game();
    const moves = ["Nf3","Nf6","Ng1","Ng8"];

    it('not draw yet', () => {
      moves.forEach(move => game.move(move));
      ok(game.current.threefold === false);
    });
    it('draw', () => {
      moves.forEach(move => game.move(move));
      ok(game.current.threefold === true);
    });
    it('draw', () => {
      const game = new Game('[fen "5K2/3q4/8/8/5k2/8/8/8 w - - 13 76"]')//new Game('[fen "3K4/8/8/8/2q2k2/8/8/8 b - - 0 69"]');
      const moves = [/*"Qg8+", "Kd7", "Qg6", "Kd8", "Qd6+", "Ke8", "Qc7", "Kf8", "Qc8+", "Ke7", "Qc7+", "Kf8",*/ "Qd7", "Kg8", "Qd8+", "Kf7", "Qd7+", "Kf6", "Qd6+", "Kf7", "Qd7+"];
  
      moves.forEach(san => {
        game.move(san)
      })
      ok(game.current.threefold !== true);
    });

  })

  describe("movetime", () => {
    const start = 600000
    const game = new Game({timecontrol: [{ start, increment: 0 }]});
    it("initial time", () => {
      ok(game.current.whitetime==start && game.current.blacktime==start)
    })
    it("within start position", () => {
      ["e4","c5"].forEach(move => game.move(move));
      ok(game.current.whitetime==start && game.current.blacktime==start)
      ok(game.withinStartRange())
    })
    it("no increment", () => {
      ["Nf3","Nc6"].forEach(move => game.move(move));
      ok(game.current.whitetime<start && game.current.blacktime<start)
    })
    it("increment", () => {
      game.timecontrol[0].increment = 5000;
      ["d4","cxd4"].forEach(move => game.move(move));
      ok(game.current.whitetime>start && game.current.blacktime>start)
    })
    it("timeout", () => {
      game.current.whitetime = 0
      strictEqual(game._status(), chessalpha.status.OUTOFTIME)

      game.position = new Position("empty")
      strictEqual(game._status(), chessalpha.status.INSUFFICIENTMATERIAL)
    })
  })

  it("prev, next & goto", () => {
    const game = new Game(pgn)
    game.goto(0)
    strictEqual(game.current, game.movelist.list[0])
    game.next()
    strictEqual(game.current, game.movelist.list[1])
    game.prev()
    strictEqual(game.current, game.movelist.list[0])
  })

  it("json", () => {
    strictEqual(new Game().toJSON().result, 0)
  })

});