import chessalpha from "../src/core"
import Position from "../src/position"
import { strictEqual, deepEqual, ok } from "assert"

const fenStart = "rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1"

/**
 * 
 * @param {String} from 
 * @param {String} to 
 * @param {Number} flags 
 * @returns {Number}
 */
function createMove(from, to, flags = 0) {
  return chessalpha.formatSquare(from)|(chessalpha.formatSquare(to)<<8)|(flags<<16)
}

describe("position", () => {

  it("constructor", () => {

    strictEqual((new Position("empty")).fen, "8/8/8/8/8/8/8/8 w - - 0 1")
    strictEqual((new Position("start")).fen, fenStart)
    strictEqual((new Position({})).fen, fenStart)
    
    const pieceList = new Array(23)
    pieceList[0] = [12,10,11,14,13,11,10,12,9,9,9,9,9,9,9,9,17,17,17,17,17,17,17,17,20,18,19,22,21,19,18,20]
    pieceList[chessalpha.WHITE|chessalpha.PAWN] = [139,138,137,136,135,134,133,132]
    pieceList[chessalpha.WHITE|chessalpha.KNIGHT] = [154,149]
    pieceList[chessalpha.WHITE|chessalpha.BISHOP] = [153,150]
    pieceList[chessalpha.WHITE|chessalpha.ROOK] = [155,148]
    pieceList[chessalpha.WHITE|chessalpha.QUEEN] = [151]
    pieceList[chessalpha.WHITE|chessalpha.KING] = [152]
    pieceList[chessalpha.BLACK|chessalpha.PAWN] = [59,58,57,56,55,54,53,52]
    pieceList[chessalpha.BLACK|chessalpha.KNIGHT] = [42,37]
    pieceList[chessalpha.BLACK|chessalpha.BISHOP] = [41,38]
    pieceList[chessalpha.BLACK|chessalpha.ROOK] = [43,36]
    pieceList[chessalpha.BLACK|chessalpha.QUEEN] = [39]
    pieceList[chessalpha.BLACK|chessalpha.KING] = [40]
    deepEqual((new Position("rnbqkbnr/pppppppp/8/8/8/8/PPPPPPPP/RNBQKBNR w KQkq - 0 1")).pieceList, pieceList)
    
    const position = new Position("empty")
    deepEqual(position, new Position(position))

    let message
    let pos
    try {
      pos = new Position(0)
    }
    catch(e) {
      message = e.message
    }
    strictEqual(message, "Position constructor error")
    strictEqual(pos, undefined)

  })

  it("get fen", () => {
    strictEqual((new Position("1k6/8/8/8/8/8/8/8 w - - 0 1")).fen, "1k6/8/8/8/8/8/8/8 w - - 0 1")
  })

  it("set fen", () => {
    deepEqual(new Position("8/8/8/8/8/8/8/8 w - - 0 1"), new Position("empty"))
    strictEqual(new Position("8/8/8/8/8/8/8/8 b - e3 0 1").fen, "8/8/8/8/8/8/8/8 b - e3 0 1")

    let pos
    let message
    try {
      pos = new Position("aa")
    }
    catch(e) {
      message = e.message
    }
    strictEqual(message, "Illegal fen")
    strictEqual(pos, undefined)
  })

  it("piecelist", () => {
    const pieceList = new Array(23)
    pieceList[0] = [chessalpha.KING|chessalpha.BLACK]
    pieceList[chessalpha.WHITE|chessalpha.PAWN] = []
    pieceList[chessalpha.WHITE|chessalpha.KNIGHT] = []
    pieceList[chessalpha.WHITE|chessalpha.BISHOP] = []
    pieceList[chessalpha.WHITE|chessalpha.ROOK] = []
    pieceList[chessalpha.WHITE|chessalpha.QUEEN] = []
    pieceList[chessalpha.WHITE|chessalpha.KING] = []
    pieceList[chessalpha.BLACK|chessalpha.PAWN] = []
    pieceList[chessalpha.BLACK|chessalpha.KNIGHT] = []
    pieceList[chessalpha.BLACK|chessalpha.BISHOP] = []
    pieceList[chessalpha.BLACK|chessalpha.ROOK] = []
    pieceList[chessalpha.BLACK|chessalpha.QUEEN] = []
    pieceList[chessalpha.BLACK|chessalpha.KING] = [chessalpha.formatSquare("a8")]
    deepEqual((new Position("k7/8/8/8/8/8/8/8 w - - 0 1")).pieceListFrom(chessalpha.formatSquare("a8")), pieceList)
  })

  describe("createmove", () => {
    it("insert", () => {
      strictEqual((new Position()).createMove(chessalpha.PAWN|chessalpha.WHITE,chessalpha.formatSquare("a8")), chessalpha.PAWN|chessalpha.WHITE|(chessalpha.formatSquare("a8")<<8))
    })
    it("e4", () => {
      const position = new Position()
      const move = chessalpha.formatSquare("e2")|(chessalpha.formatSquare("e4")<<8)
      strictEqual(position.createMove(move), move)
    })
    it("O-O", () => {
      const position = new Position("rnbqkbnr/pp2pppp/8/3p4/2p5/5NP1/PPPPPPBP/RNBQK2R w - d6 0 4")
      const move = createMove("e1","g1")
      strictEqual(position.createMove(move), move|(chessalpha.CastleKing)<<16)
    })
    it("O-O-0", () => {
      const position = new Position("rnbqkbnr/pp2pppp/8/3p4/2p5/5NP1/PPPPPPBP/R3K2R w - d6 0 4")
      const move = createMove("e1","c1")
      strictEqual(position.createMove(move), move|(chessalpha.CastleQueen)<<16)
    })
    it("epc", () => {
      const position = new Position("rnbqkbnr/pp2pppp/8/3p4/2pP4/5NP1/PPP1PPBP/RNBQK2R b KQkq d3 0 4")
      const move = createMove("c4","d3")
      strictEqual(position.createMove(move), move|(chessalpha.EPC<<16))
    })
    it("promote", () => {
      const position = new Position("rnbqkbnr/pp2pppp/8/3p4/3P1B2/5NP1/PpP1PPBP/RN1Q1RK1 b kq - 1 6")
      const move = createMove("b2","a1")
      strictEqual(position.createMove(move), move|(chessalpha.PromoteQueen<<16))
      strictEqual(position.createMove(move|(chessalpha.PromoteQueen<<16)), move|(chessalpha.PromoteQueen<<16))
    })
  })

  describe("movegenerator", () => {
    it("pieces", () => {
      deepEqual((new Position("8/8/8/8/8/8/3P4/8 w - -"))._quiteMovesPawn().sort().sort(),[createMove("d2","d3"), createMove("d2","d4")].sort())
      deepEqual((new Position("8/8/8/2p5/3P4/8/8/8 w - -"))._captureMovesPawn().sort(),[createMove("d4","c5")].sort())

      deepEqual((new Position("8/8/8/8/3N4/8/8/8 w - -"))._quiteMovesKnight().sort(),[createMove("d4","c2"), createMove("d4","e2"), createMove("d4","b3"), createMove("d4","f5"), createMove("d4","e6"), createMove("d4","c6"), createMove("d4","f3"), createMove("d4","b5")].sort())
      deepEqual((new Position("8/8/2p1p3/1p3p2/3N4/1p3p2/2p1p3/8 w - -"))._captureMovesKnight().sort(),[createMove("d4","c2"), createMove("d4","e2"), createMove("d4","b3"), createMove("d4","f5"), createMove("d4","e6"), createMove("d4","c6"), createMove("d4","f3"), createMove("d4","b5")].sort())

      deepEqual((new Position("8/8/8/8/3B4/8/8/8 w - -"))._quiteMovesBishop().sort(),[createMove("d4","e5"), createMove("d4","f6"), createMove("d4","g7"), createMove("d4","h8"), createMove("d4","c5"), createMove("d4","b6"), createMove("d4","a7"), createMove("d4","c3"), createMove("d4","b2"), createMove("d4","a1"), createMove("d4","e3"), createMove("d4","f2"), createMove("d4","g1")].sort())
      deepEqual((new Position("8/8/8/2p1p3/3B4/2p1p3/8/8 w - -"))._captureMovesBishop().sort(),[createMove("d4","e5"), createMove("d4","c5"), createMove("d4","c3"), createMove("d4","e3")].sort())

      deepEqual((new Position("8/8/8/8/3R4/8/8/8 w - -"))._quiteMovesRook().sort(),[createMove("d4","c4"), createMove("d4","b4"), createMove("d4","a4"), createMove("d4","e4"), createMove("d4","f4"), createMove("d4","g4"), createMove("d4","h4"), createMove("d4","d3"), createMove("d4","d2"), createMove("d4","d1"), createMove("d4","d5"), createMove("d4","d6"), createMove("d4","d7"), createMove("d4","d8")].sort())
      deepEqual((new Position("8/8/8/3p4/2pRp3/3p4/8/8 w - -"))._captureMovesRook().sort(),[createMove("d4","c4"), createMove("d4","e4"), createMove("d4","d5"), createMove("d4","d3")].sort())

      deepEqual((new Position("8/8/8/8/3Q4/8/8/8 w - -"))._quiteMovesQueen().sort(),[createMove("d4","e5"), createMove("d4","f6"), createMove("d4","g7"), createMove("d4","h8"), createMove("d4","c5"), createMove("d4","b6"), createMove("d4","a7"), createMove("d4","c3"), createMove("d4","b2"), createMove("d4","a1"), createMove("d4","e3"), createMove("d4","f2"), createMove("d4","g1"), createMove("d4","c4"), createMove("d4","b4"), createMove("d4","a4"), createMove("d4","e4"), createMove("d4","f4"), createMove("d4","g4"), createMove("d4","h4"), createMove("d4","d3"), createMove("d4","d2"), createMove("d4","d1"), createMove("d4","d5"), createMove("d4","d6"), createMove("d4","d7"), createMove("d4","d8")].sort())
      deepEqual((new Position("8/8/8/8/2pQ4/8/8/8 w - -"))._captureMovesQueen().sort(),[createMove("d4","c4")].sort())

      deepEqual((new Position("8/8/8/8/3K4/8/8/8 w - -"))._quiteMovesKing().sort(),[createMove("d4","e5"), createMove("d4","c5"), createMove("d4","c3"), createMove("d4","e3"), createMove("d4","c4"), createMove("d4","e4"), createMove("d4","d5"), createMove("d4","d3")].sort())
      deepEqual((new Position("8/8/8/2ppp3/2pKp3/2ppp3/8/8 w - -"))._captureMovesKing().sort(),[createMove("d4","e5"), createMove("d4","c5"), createMove("d4","c3"), createMove("d4","e3"), createMove("d4","c4"), createMove("d4","e4"), createMove("d4","d5"), createMove("d4","d3")].sort())
    })
    it("start", () => {
      deepEqual((new Position("start")).validMoves.sort(),[createMove("h2","h3"), createMove("h2","h4"), createMove("g2","g3"), createMove("g2","g4"), createMove("f2","f3"), createMove("f2","f4"), createMove("e2","e3"), createMove("e2","e4"), createMove("d2","d3"), createMove("d2","d4"), createMove("c2","c3"), createMove("c2","c4"), createMove("b2","b3"), createMove("b2","b4"), createMove("a2","a3"), createMove("a2","a4"), createMove("g1","h3"), createMove("g1","f3"), createMove("b1","c3"), createMove("b1","a3")].sort())
    })
    it("castle", () => {
      deepEqual((new Position("r3k2r/p1K1Bp1p/P4P1P/8/8/8/8/8 b kq -"))._allMoves(chessalpha.formatSquare("e8")).sort(),[createMove("e8","d7"), createMove("e8","d8"), createMove("e8","f8"), createMove("e8","g8","4"), createMove("e8","c8","8"), createMove("e8","e7")].sort())
      deepEqual((new Position("r3k2r/p1K1Pp1p/P2B1P1P/8/8/8/8/8 b k -")).allMoves.sort(),[createMove("h8","g8"), createMove("h8","f8"), createMove("a8","b8"), createMove("a8","c8"), createMove("a8","d8"), createMove("e8","d7"), createMove("e8","d8"), createMove("e8","f8"), createMove("e8","g8",4), createMove("e8","e7")].sort())
      deepEqual((new Position("r3k2r/p1K1Pp1p/P2B1P1P/8/8/8/8/8 b k -")).quiteMoves.sort(),[createMove("h8","g8"), createMove("h8","f8"), createMove("a8","b8"), createMove("a8","c8"), createMove("a8","d8"), createMove("e8","d7"), createMove("e8","d8"), createMove("e8","f8"), createMove("e8","g8",4)].sort())
      deepEqual((new Position("r3k2r/p1K1Pp1p/P2B1P1P/8/8/8/8/8 b k -")).captureMoves.sort(),[createMove("e8","e7")].sort())

      // // king in check
      deepEqual((new Position("4k2r/2K2p1p/2B2P1P/8/8/8/8/8 b k -")).validMoves.sort(),[createMove("e8","f8")].sort())

      // king crosses threatened square
      deepEqual((new Position("r3k2r/p1K1Bp1p/P4P1P/8/8/8/8/8 b kq -")).validMoves.sort(),[createMove("h8","g8"), createMove("h8","f8"), createMove("a8","b8"), createMove("a8","c8"), createMove("a8","d8")].sort())
    })
  })

  it("move", () => {
    // Smith-Morra Gambit
    strictEqual(new Position().
        move("e4").
        move("c5").
        move("d4").
        fen, "rnbqkbnr/pp1ppppp/8/2p5/3PP3/8/PPP2PPP/RNBQKBNR b KQkq d3 0 2")

    // Castle
    strictEqual(new Position().
        move("Nf3").
        move("e6").
        move("g3").
        move("Nf6").
        move("Bg2").
        move("Bc5").
        move("O-O").
        move("O-O").
        fen, "rnbq1rk1/pppp1ppp/4pn2/2b5/8/5NP1/PPPPPPBP/RNBQ1RK1 w - - 0 5")
    strictEqual(new Position("r3k3/pppppppp/8/8/8/8/PPPPPPPP/R3K3 w KQ - 0 0").
        move("0-0-0").
        move("O-O-O").
        fen, "2kr4/pppppppp/8/8/8/8/PPPPPPPP/2KR4 w - - 0 1")

    // 0 move
    strictEqual(new Position().
        move(0).
        fen, fenStart.replace(" 0 ", " 1 ").replace(" w ", " b "))

    // 1 move
    strictEqual(new Position().
        move(1).
        fen, fenStart.replace(" 0 ", " 1 "))

    // replace piece
    strictEqual(new Position("p7/8/8/8/8/8/8/8 w - - 0 0").
        move(chessalpha.WHITE|chessalpha.PAWN|(chessalpha.formatSquare("a8")<<8)).
        fen, "P7/8/8/8/8/8/8/8 b - - 0 1")
    
    // remove piece
    strictEqual(new Position("p7/8/8/8/8/8/8/8 w - - 0 0").
        move(chessalpha.formatSquare("a8")).
        fen, "8/8/8/8/8/8/8/8 b - - 0 1")

    // en passant
    strictEqual(new Position("8/8/8/pP6/8/8/8/8 w - a6 0 0").
        move("bxa6").
        fen, "8/8/P7/8/8/8/8/8 b - - 0 1")

    // promote
    strictEqual(new Position("8/P7/8/8/8/8/8/8 w - - 0 0").
        move("a8=Q").
        fen, "Q7/8/8/8/8/8/8/8 b - - 0 1")
    strictEqual(new Position("8/P7/8/8/8/8/8/8 w - - 0 0").
        move("a8=R").
        fen, "R7/8/8/8/8/8/8/8 b - - 0 1")
    strictEqual(new Position("8/P7/8/8/8/8/8/8 w - - 0 0").
        move("a8=B").
        fen, "B7/8/8/8/8/8/8/8 b - - 0 1")
    strictEqual(new Position("8/P7/8/8/8/8/8/8 w - - 0 0").
        move("a8=N").
        fen, "N7/8/8/8/8/8/8/8 b - - 0 1")

    // line & row san
    strictEqual(new Position("B1B5/8/B7/8/8/8/8/8 w - - 0 0").
        move("Ba8b7").
        fen, "2B5/1B6/B7/8/8/8/8/8 b - - 1 1")
    strictEqual(new Position("B7/8/B7/8/8/8/8/8 w - - 0 0").
        move("B8b7").
        fen, "8/1B6/B7/8/8/8/8/8 b - - 1 1")
  })

  it("san", () => {
    // unvalid San
    ok(!new Position().sanToMove("Ke4"))

    strictEqual(new Position().moveToSan(0), '--')
    strictEqual(new Position().moveToSan(1), '*')
    strictEqual(new Position().moveToSan(chessalpha.WHITE|chessalpha.KNIGHT|(chessalpha.formatSquare("f3")<<8)), 'N@f3')
    strictEqual(new Position().moveToSan(chessalpha.formatSquare("g1")), '-g1')
    strictEqual(new Position().moveToSan(createMove("g1","g4")), 'g1g4')
    strictEqual(new Position().moveToSan(createMove("g2","g8",chessalpha.PromoteQueen)), 'g2g8=Q')
    strictEqual(new Position().moveToSan(createMove("g1","f3")), 'Nf3')

    strictEqual(new Position("r3k2r/pppppppp/8/8/8/8/8/8 b kq - 0 0").moveToSan(createMove("e8","g8",chessalpha.CastleKing)), "O-O")
    strictEqual(new Position("r3k2r/pppppppp/8/8/8/8/8/8 b kq - 0 0").moveToSan(createMove("e8","c8",chessalpha.CastleQueen)), "O-O-O")

    strictEqual(new Position("B1B5/8/B7/8/8/8/8/8 w - - 0 0").moveToSan(createMove("a8","b7")), "Ba8b7")
    strictEqual(new Position("B7/8/B7/8/8/8/8/8 w - - 0 0").moveToSan(createMove("a8","b7")), "B8b7")
    strictEqual(new Position("B1B5/8/8/8/8/8/8/8 w - - 0 0").moveToSan(createMove("a8","b7")), "Bab7")

    strictEqual(new Position("8/8/8/Pp6/8/8/8/8 w - b6 0 0").moveToSan(createMove("a5","b6",chessalpha.EPC)), "axb6")
    strictEqual(new Position("8/8/1p6/P7/8/8/8/8 w - b6 0 0").moveToSan(createMove("a5","b6")), "axb6")

    strictEqual(new Position("8/P7/8/8/8/8/8 w - - 0 0").moveToSan(createMove("a7","a8",chessalpha.PromoteQueen)), "a8=Q")

    strictEqual(new Position("8/Pk7/8/8/8/8/8 w - - 0 0").moveToSan(createMove("a7","a8",chessalpha.PromoteQueen)), "a8=Q+")
    strictEqual(new Position("k7/1PK5/8/8/8/8/8 w - - 0 0").moveToSan(createMove("b7","b8",chessalpha.PromoteQueen)), "b8=Q#")
  })

  it("checkmate", () => {
    strictEqual((new Position("kQK5/8/8/8/8/8/8/8 b - -")).checkmate, true)
  })

  it("stalemate", () => {
    strictEqual((new Position("k1K5/1R6/8/8/8/8/8/8 b - -")).stalemate, true)
  })

  it("status", () => {
    strictEqual((new Position("k1K5/8/8/8/8/8/8/8 b - -")).status, chessalpha.status.INSUFFICIENTMATERIAL)
    strictEqual((new Position("kQK5/8/8/8/8/8/8/8 b - -")).status, chessalpha.status.MATE)
    strictEqual((new Position("k1K5/1R6/8/8/8/8/8/8 b - -")).status, chessalpha.status.STALEMATE)
    strictEqual((new Position("k1K5/1R6/8/8/8/8/8/8 w - - 101 120")).status, chessalpha.status.MOVE50)
    strictEqual((new Position("k1K5/1R6/8/8/8/8/8/8 w - -")).status, 0)
  })

  describe("insufficient material", () => {
    [{
      name: "k vs. k",
      fen: "k7/8/8/8/8/8/8/K7 w - - 0 0",
      assert: true
    },{
      name: "kn vs. k",
      fen: "kn6/8/8/8/8/8/8/K7 w - - 0 0",
      assert: true
    },{
      name: "kb vs. k",
      fen: "kb6/8/8/8/8/8/8/K7 w - - 0 0",
      assert: true
    },{
      name: "kb vs. kb same color",
      fen: "kb6/8/8/8/8/8/8/K7 w - - 0 0",
      assert: true
    },{
      name: "kb vs. kb different color",
      fen: "kb6/8/8/8/8/8/8/KB6 w - - 0 0",
      assert: false
    },{
      name: "start position",
      fen: "start",
      assert: false
    }].forEach(test => {
      it(test.name, () => {
        strictEqual((new Position(test.fen)).insufficientMaterial, test.assert)
      })
    })
  })

  it("ascii", () => {
    strictEqual(new Position().ascii, `
   +------------------------+
 8 | r  n  b  q  k  b  n  r |
 7 | p  p  p  p  p  p  p  p |
 6 | .  .  .  .  .  .  .  . |
 5 | .  .  .  .  .  .  .  . |
 4 | .  .  .  .  .  .  .  . |
 3 | .  .  .  .  .  .  .  . |
 2 | P  P  P  P  P  P  P  P |
 1 | R  N  B  Q  K  B  N  R |
   +------------------------+
     a  b  c  d  e  f  g  h
`)
  })

  it("key", () => {
    strictEqual(new Position().key, "rnbqkbnrpppppppp--------------------------------PPPPPPPPRNBQKBNRplw")
  })

  it("hash", () => {
    ok(new Position().hash.equals(new Position().hash))
    strictEqual(new Position().hash.toString(), "-1278739575,-32639496")
  })

  
  // it("pieceList", () => {
  //   deepEqual(position.pieceList, [[12,10,11,14,13,11,10,12,9,9,9,9,9,9,9,9,17,17,17,17,17,17,17,17,20,18,19,22,21,19,18,20],,,,,,,,,[139,138,137,136,135,134,133,132],[154,149],[153,150],[155,148],[151],[152],,,[59,58,57,56,55,54,53,52],[42,37],[41,38],[43,36],[39],[40]])
  // })

})