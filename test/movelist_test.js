import { strictEqual, deepEqual, ok } from "assert"
import chessalpha from '../src/core.js'
import { Movelist } from '../src/movelist.js'
import Position from '../src/position.js'

const e2e4 = chessalpha.formatSquare("e2")|(chessalpha.formatSquare("e4")<<8)
const d2d4 = chessalpha.formatSquare("d2")|(chessalpha.formatSquare("d4")<<8)
const e7e5 = chessalpha.formatSquare("e7")|(chessalpha.formatSquare("e5")<<8)

/**
 * Generates a default movelist
 * 
 * @returns {Movelist}
 */
function generateMovelist() {
  return new Movelist([{id: 0, position:'start', start: true, next: [1]},{id: 1, move:e2e4, next: [2]},{id: 2, move:e7e5}])
}

describe('movelist', () => {

  it("constructor", () => {
    const movelist = generateMovelist()
    strictEqual(movelist.list.length, 3)
    strictEqual(new Movelist(movelist.toJSON()).list.length, 3)
  })

  it("position", () => {
    const movelist = generateMovelist()
    deepEqual(movelist.position(movelist.list[0]), new Position())
  })

  it("last & first", () => {
    const movelist = generateMovelist()
    deepEqual(movelist.first(), movelist.list[0])
    deepEqual(movelist.first(movelist.list[1]), movelist.list[0])
    deepEqual(movelist.last(movelist.list[1]), movelist.list[2])
    deepEqual(movelist.last(), movelist.list[2])
  })

  it("move", () => {
    const movelist = generateMovelist()

    // move already exists
    movelist.move(e2e4, movelist.first(), "e4")
    strictEqual(movelist.list.length, 3)

    movelist.move(d2d4, movelist.last(), "d4")
    strictEqual(movelist.list.length, 4)

    movelist.move(d2d4, movelist.first(), "d4", 0)
    strictEqual(movelist.list.length, 5)
  })

  it("remove", () => {
    const movelist = generateMovelist()

    movelist.remove(1)
    strictEqual(movelist.list.length, 3)
    strictEqual(movelist.list[1], undefined)
    strictEqual(movelist.list[2], undefined)
  })

  it("merge", () => {
    const movelist = generateMovelist()

    const movelist2 = new Movelist([{id: 0, position:'empty'}])
    movelist.merge(movelist.first(), movelist2.first())
    strictEqual(movelist.list.length, 3)

    const movelist3 = new Movelist([{id: 0, position:'start', start: true, next: [1]},{id: 1, move:d2d4}])
    movelist.merge(movelist.first(), movelist3.first(), true)
    strictEqual(movelist.list.length, 4)

    const movelist4 = new Movelist([{id: 0, position:'start', start: true, next: [1]},{id: 1, move:d2d4}, {id: 2, move:e7e5}])
    movelist.merge(movelist.first(), movelist4.first())
    strictEqual(movelist.list.length, 5)
  })

  describe("node", () => {
    const movelist = generateMovelist()

    ok(!movelist.first().equals(movelist.last()))

    strictEqual(movelist.last().prev.movelist.list.length, 2)

    movelist.move(d2d4, movelist.first(), "d4")
    ok(movelist.last().mainline)
    ok(!movelist.first().next[1].mainline)
  })


})