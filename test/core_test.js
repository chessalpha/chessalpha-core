import chessalpha from '../src/core'
import { strictEqual, deepEqual } from 'assert'

describe('converters', () => {

  it('square', () => {
    strictEqual(chessalpha.formatSquare("a8"), 36)
    strictEqual(chessalpha.formatSan(36), "a8")
    strictEqual(chessalpha.format64To192(0), 36)
    strictEqual(chessalpha.format192To64(36), 0)
    deepEqual(chessalpha.formatXY(36), { x: 0, y: 0 })
    strictEqual(chessalpha.formatXYToSan({ x: 0, y: 0 }), "a8")
    strictEqual(chessalpha.formatXYToSquare({ x: 0, y: 0 }), 36)
    deepEqual(chessalpha.formatSanToXY("a8"), { x: 0, y: 0 })
  })

  it('piece', () => {
    strictEqual(chessalpha.formatPiece("knight"), chessalpha.KNIGHT)
    strictEqual(chessalpha.formatPiece("knight","white"), chessalpha.KNIGHT|chessalpha.WHITE)
    strictEqual(chessalpha.formatPieceToChar(chessalpha.KNIGHT|chessalpha.WHITE), "n")
    strictEqual(chessalpha.formatPieceToFen(chessalpha.KNIGHT|chessalpha.WHITE), "N")
    strictEqual(chessalpha.formatPieceToFen(chessalpha.KING|chessalpha.BLACK), "k")
    strictEqual(chessalpha.formatPieceToFen(), "-")
    strictEqual(chessalpha.formatPieceToHash(chessalpha.KNIGHT|chessalpha.WHITE), 1)
    strictEqual(chessalpha.formatCharToPiece("n"), chessalpha.KNIGHT)
    strictEqual(chessalpha.formatPiecestring(chessalpha.KNIGHT), "knight")
  })

  it('color', () => {
    strictEqual(chessalpha.formatColorstring(chessalpha.WHITE), "white")
    strictEqual(chessalpha.formatColorstring(chessalpha.BLACK), "black")
    strictEqual(chessalpha.formatColor("white"), chessalpha.WHITE)
    strictEqual(chessalpha.formatColor("Black"), chessalpha.BLACK)
  })

  it('result', () => {
    strictEqual(chessalpha.formatResult("*"), 0)
    strictEqual(chessalpha.formatResult("1/2-1/2"), chessalpha.result.DRAW)
    strictEqual(chessalpha.formatResult("½-½"), chessalpha.result.DRAW)
    strictEqual(chessalpha.formatResult("1-0"), chessalpha.result.WHITEWIN)
    strictEqual(chessalpha.formatResult("0-1"), chessalpha.result.BLACKWIN)
    strictEqual(chessalpha.formatResultstring(chessalpha.result.DRAW), "½-½")
    strictEqual(chessalpha.formatResultstring(chessalpha.result.WHITEWIN), "1-0")
    strictEqual(chessalpha.formatResultstring(chessalpha.result.BLACKWIN), "0-1")
  })

})